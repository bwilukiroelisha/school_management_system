<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ClassController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\HealthMeasurementController;
use App\Http\Controllers\Admin\NoticeBoardController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\SubjectController;
use App\Http\Controllers\Admin\ClassSubjectController;
use App\Http\Controllers\Admin\ClassTimeTableController;
use App\Http\Controllers\Admin\CommunicateController;
use App\Http\Controllers\Admin\AssignmentController;
use App\Http\Controllers\Admin\GradeController;
use App\Http\Controllers\Admin\HealthRecordController;
use App\Http\Controllers\Admin\ParentController;
use App\Http\Controllers\Admin\TeacherController;
use App\Http\Controllers\Admin\TeacherSubjectController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;



## AUTHENTICATION
Route::get('/', [AuthController::class, 'index']);
Route::post('/',[AuthController::class, 'login']);
Route::get('logout', [AuthController::class, 'logout']);
Route::get('forgot-password', [AuthController::class, 'forgot_password']);
Route::post('forgot-password', [AuthController::class, 'Postforgot_password']);

## PROFILE
Route::get('user/profile', [AuthController::class, 'profile']);
Route::post('user/profile', [AuthController::class, 'update_password']);
Route::get('user/role', [AuthController::class, 'userRole']);


## ADMIN
Route::group(['middleware' => 'admin'], function () {

    Route::prefix('admin')->group(function () {

        ## Dashboard
        Route::get('/dashboard', [DashboardController::class, 'index']);

        ## Admin
        Route::get('/admin/list', [AdminController::class, 'list']);
        Route::get('/admin/list_view', [AdminController::class, 'listView']);
        Route::post('/admin/add_admin', [AdminController::class, 'addAdmin']);
        Route::get('/admin/delete_admin/{id}', [AdminController::class, 'deleteAdmin']);
        Route::get('/admin/edit_admin/{id}', [AdminController::class, 'editAdmin']);

        ## Student
        Route::get('/student/list', [StudentController::class, 'list']);
        Route::get('/student/list_view', [StudentController::class, 'listView']);
        Route::post('/student/add', [StudentController::class, 'add']);
        Route::get('/student/edit/{id}', [StudentController::class, 'edit']);
        Route::get('/student/delete/{id}', [StudentController::class, 'delete']);

        ## Teacher
        Route::get('/teacher/list', [TeacherController::class, 'list']);
        Route::get('/teacher/list_view', [TeacherController::class, 'listView']);
        Route::post('/teacher/add', [TeacherController::class, 'add']);
        Route::get('/teacher/delete/{id}', [TeacherController::class, 'delete']);
        Route::get('/teacher/edit/{id}', [TeacherController::class, 'edit']);
        Route::get('/teacher/my_subject/{id}', [TeacherController::class, 'mySubject']);
        Route::get('/teacher/my_subject_view/{id}', [TeacherController::class, 'mySubjectView']);
        Route::post('/teacher/addMySubject', [TeacherController::class, 'addMySubject']);
        Route::get('/teacher/deleteTeacherSubject/{id}', [TeacherController::class, 'deleteTeacherSubject']);

        ## Parent
        Route::get('/parent/list', [ParentController::class, 'list']);
        Route::get('/parent/list_view', [ParentController::class, 'listView']);
        Route::post('/parent/add', [ParentController::class, 'add']);
        Route::get('/parent/delete/{id}', [ParentController::class, 'delete']);
        Route::get('/parent/edit/{id}', [ParentController::class, 'edit']);
        Route::get('/parent/my_student/{id}', [ParentController::class, 'myStudent']);
        Route::get('/parent/my_student_view/{id}', [ParentController::class, 'myStudentView']);
        Route::get('/parent/searchStudent/{text}', [ParentController::class, 'searchStudent']);
        Route::post('/parent/addMyStudent', [ParentController::class, 'addMyStudent']);
        Route::get('/parent/deleteParentStudent/{id}', [ParentController::class, 'deleteParentStudent']);


        ## Class
        Route::get('/class/list', [ClassController::class, 'list']);
        Route::get('/class/list_view', [ClassController::class, 'listView']);
        Route::post('/class/add', [ClassController::class, 'add']);
        Route::get('/class/delete/{id}', [ClassController::class, 'delete']);
        Route::get('/class/edit/{id}', [ClassController::class, 'edit']);

        ## Subject
        Route::get('/subject/list', [SubjectController::class, 'list']);
        Route::get('/subject/list_view', [SubjectController::class, 'listView']);
        Route::post('/subject/add', [SubjectController::class, 'add']);
        Route::get('/subject/delete/{id}', [SubjectController::class, 'delete']);
        Route::get('/subject/edit/{id}', [SubjectController::class, 'edit']);

        ## Class Timetable
        Route::get('/timetable/list', [ClassTimeTableController::class, 'list']);
        Route::get('/timetable/list_view', [ClassTimeTableController::class, 'listView']);
        Route::post('/timetable/add', [ClassTimeTableController::class, 'add']);
        Route::get('/timetable/delete/{id}', [ClassTimeTableController::class, 'delete']);
        Route::get('/timetable/edit/{id}', [ClassTimeTableController::class, 'edit']);

        ## Exam Schedule
        Route::get('/exam_schedule/list', [ClassTimeTableController::class, 'exam']);
        Route::get('/exam_schedule/list_view', [ClassTimeTableController::class, 'examView']);
        Route::post('/exam_schedule/add', [ClassTimeTableController::class, 'saveExam']);
        Route::get('/exam_schedule/delete/{id}', [ClassTimeTableController::class, 'deleteExam']);
        Route::get('/exam_schedule/edit/{id}', [ClassTimeTableController::class, 'editExam']);

        ## Grades
        Route::get('/grade/list', [GradeController::class, 'list']);
        Route::get('/grade/list_view', [GradeController::class, 'listView']);
        Route::post('/grade/add', [GradeController::class, 'add']);
        Route::get('/grade/edit/{id}', [GradeController::class, 'edit']);
        Route::get('/grade/delete/{id}', [GradeController::class, 'delete']);

        ## Notice Board
        Route::get('/notice_board/list', [NoticeBoardController::class, 'list']);
        Route::get('/notice_board/list_view', [NoticeBoardController::class, 'listView']);
        Route::post('/notice_board/add', [NoticeBoardController::class, 'add']);
        Route::get('/notice_board/edit/{id}', [NoticeBoardController::class, 'edit']);
        Route::get('/notice_board/delete/{id}', [NoticeBoardController::class, 'delete']);

        ## Communication
        Route::get('/communicate/list', [CommunicateController::class, 'list']);
        Route::get('/communicate/list_view', [CommunicateController::class, 'listView']);
        Route::post('/communicate/add', [CommunicateController::class, 'add']);
        Route::get('/communicate/delete/{id}', [CommunicateController::class, 'delete']);
        Route::get('/communicate/edit/{id}', [CommunicateController::class, 'edit']);
        Route::get('/communicate/read_mail/{id}', [CommunicateController::class, 'readMail']);

        ## Health Measurement
        Route::get('/health_measurement/list', [HealthMeasurementController::class, 'list']);
        Route::get('/health_measurement/list_view', [HealthMeasurementController::class, 'listView']);
        Route::post('/health_measurement/add', [HealthMeasurementController::class, 'addList']);
        Route::get('/health_measurement/edit/{id}', [HealthMeasurementController::class, 'editList']);
        Route::get('/health_measurement/delete/{id}', [HealthMeasurementController::class, 'deleteList']);

        ## Health Record
        Route::get('/health_record/list', [HealthRecordController::class, 'list']);
        Route::get('/health_record/list_view', [HealthRecordController::class, 'listView']);
        Route::post('/health_record/add', [HealthRecordController::class, 'addList']);
        Route::get('/health_record/edit/{id}', [HealthRecordController::class, 'editList']);
        Route::get('/health_record/delete/{id}', [HealthRecordController::class, 'deleteList']);

        ## Assign Student => Subject
        Route::get('/student_subject/list', [StudentController::class, 'studentSubject']);
        Route::get('/student_subject/list_view', [StudentController::class, 'studentSubjectView']);
        Route::post('/student_subject/add', [StudentController::class, 'addStudentSubject']);
        Route::get('/student_subject/edit/{id}', [StudentController::class, 'editStudentSubject']);
        Route::get('/student_subject/delete/{id}', [StudentController::class, 'deleteStudentSubject']);


        ## Assign Subject => Class
        Route::get('/class_subject/list', [ClassSubjectController::class, 'list']);
        Route::get('/class_subject/list_view', [ClassSubjectController::class, 'listView']);
        Route::post('/class_subject/add', [ClassSubjectController::class, 'add']);
        Route::get('/class_subject/delete/{id}', [ClassSubjectController::class, 'delete']);
        Route::get('/class_subject/edit/{id}', [ClassSubjectController::class, 'edit']);

        ## Assign Teacher => Subject
        Route::get('/teacher_subject/list', [TeacherSubjectController::class, 'list']);
        Route::get('/teacher_subject/list_view', [TeacherSubjectController::class, 'listView']);
        Route::post('/teacher_subject/add', [TeacherSubjectController::class, 'add']);
        Route::get('/teacher_subject/delete/{id}', [TeacherSubjectController::class, 'delete']);
        Route::get('/teacher_subject/edit/{id}', [TeacherSubjectController::class, 'edit']);


    });

});


## TEACHER
Route::group(['middleware'=>'teacher'], function () {

    Route::prefix('teacher')->group(function () {

        ## Dashboard
        Route::get('/dashboard', [DashboardController::class, 'index']);

        ## Student
        Route::get('/student/list', [TeacherController::class, 'myStudent']);
        Route::get('/student/list_view', [TeacherController::class, 'myStudentView']);
        Route::get('/student/update/{id}', [TeacherController::class, 'myStudentUpdate']);
        Route::post('/marks/add/{subject_id}/{student_id}', [TeacherController::class, 'addMarks']);
        Route::get('/marks/edit/{id}/{subject_id}/{student_id}', [TeacherController::class, 'editMarks']);

        ## Subject
        Route::get('/subject/list', [SubjectController::class, 'mySubject']);
        Route::get('/subject/list_view', [SubjectController::class, 'mySubjectView']);
        Route::get('/class_student/{id}', [TeacherSubjectController::class, 'classStudent']);
        Route::get('/class_student/list_view/{id}', [TeacherSubjectController::class, 'classStudentView']);
        Route::get('/view_timetable/{id}', [ClassTimeTableController::class, 'viewTimetable']);

        ## Assignment
        Route::get('/assignment/list', [AssignmentController::class, 'list']);
        Route::get('/assignment/list_view', [AssignmentController::class, 'listView']);
        Route::post('/assignment/add', [AssignmentController::class, 'add']);
        Route::get('/assignment/delete/{id}', [AssignmentController::class, 'delete']);
        Route::get('/assignment/edit/{id}', [AssignmentController::class, 'edit']);

        ## Exam Schedule
        Route::get('/exam_schedule/list', [ClassTimeTableController::class, 'exam']);
        Route::get('/exam_schedule/list_view', [ClassTimeTableController::class, 'examView']);

        ## Class Timetable
        Route::get('/timetable/list', [ClassTimeTableController::class, 'list']);
        Route::get('/timetable/list_view', [ClassTimeTableController::class, 'listView']);




    });
});


## STUDENT
Route::group(['middleware' => 'student'], function () {

    Route::prefix('student')->group(function () {
        ## Dashboard
        Route::get('/dashboard', [DashboardController::class, 'index']);

        ## Subject
        Route::get('/subject/list', [SubjectController::class, 'studentSubject']);
        Route::get('/subject/list_view', [SubjectController::class, 'studentSubjectView']);

        ## Result
        Route::get('/results/list', [SubjectController::class, 'studentResults']);
        Route::get('/results/list_view', [SubjectController::class, 'studentResultsView']);
        Route::get('/print/results/{id}', [SubjectController::class, 'printResults']);

        ## Grades
        Route::get('/grade/list', [GradeController::class, 'studentGrade']);
        Route::get('/grade/list_view', [GradeController::class, 'studentGradeView']);

        ## Exam Schedule
        Route::get('/exam_schedule/list', [ClassTimeTableController::class, 'exam']);
        Route::get('/exam_schedule/list_view', [ClassTimeTableController::class, 'examView']);

        ## Class Timetable
        Route::get('/timetable/list', [ClassTimeTableController::class, 'list']);
        Route::get('/timetable/list_view', [ClassTimeTableController::class, 'listView']);


    });
});


## PARENT
Route::group(['middleware'=>'parent'], function () {

    Route::prefix('parent')->group(function () {

        ## Dashboard
        Route::get('/dashboard', [DashboardController::class, 'index']);

    });


});







