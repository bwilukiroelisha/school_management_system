


<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>

<script>

// $(document).ready(function () {

$('textarea.summernote').summernote({
    placeholder: 'Enter contents...',
    tabsize: 2,
    height: 300,
    minHeight: null,
    maxHeight: null,
    focus: true
});
// });


$('.duallistbox').bootstrapDualListbox();
$('.select2').select2()


</script>
