

@extends('layouts.app')
@section('content')
@include('dependences.style')

@include('layouts.sub_header')


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">

            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>

            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>

            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>





<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="">Student <span class="text-danger">*</span></label>
                            <select name="student_id" id="student_id" class="form-control select2" style="width: 100%;" required>
                                <option value="">Search student</option>
                                <?php foreach ($students as $student) {?>
                                <option value="<?=$student->id?>"><?=$student->fname.' '.$student->mname.' '.$student->lname.' ( '.$student->admission_no.' ) '?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php foreach ($measurements as $measurement) { ?>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="measurement_<?=$measurement->id?>"><?=$measurement->name?> <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" id="measurement_id" name="<?=$measurement->id?>" class="form-control" placeholder="Enter <?=strtolower($measurement->name)?>" required>
                                <small class="text-danger"><?=$measurement->remark?></small>
                            </div>
                        </div>
                        <?php }?>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label class="form-label">Status <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required>
                                <option>Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                      <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@include('dependences.script')


<script>
$(document).ready(function () {
    getView();
    closeModel();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('admin/health_record/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}

function closeModel(){
    $('#modal-lg').modal('hide');
}

function deleteHRecord(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
        return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/health_record/delete/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }
            }
    });
}

function editHRecord(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');
    var input = '';

    jQuery.ajax({
            type: "GET",
            url: "/admin/health_record/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                // $("#student_id").val(rowData.student_id);
                // $("#measurement_id").val(rowData.measurement_id);

                // $.each(rowData,function(index,data){
                //     input += "<input value="+rowData.value+">";
                // });

                // $("#measurement_id").append(input);
                // $("#status").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/health_record/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                closeModel();
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('.alert').fadeOut(7000);
                closeModel();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
