
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Admission No.</th>
                <th>Subjects</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Admission No.</th>
                <th>Subjects</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $studentId => $subjects) {
                $student = $subjects->first();
                $user = App\Models\AdminModel::findAdmin($student->created_by);
                // dd($student);
                ?>

            <tr>
                <td><?=$n?></td>
                <td>
                    <div>
                        <?php if(!empty($student->image)){ ?>
                        <img src="{{ asset('upload/profile/'.$student->image) }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php }else{ ?>
                        <img src="{{ asset('assets/avatar.jpg') }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php } ?>
                        <small class="p-2 text-muted"><?= $student->gender ?></small>
                    </div>
                    <p class="c_name"><?= $student->fname.' '.$student->mname.' '.$student->lname ?></p>
                </td>
                <td>
                    <span><a href="tel:<?= $student->phone ?>" class="text-dark mb-2"><?= $student->phone ?></a></span>
                    <p class="mt-2"><a href="mailto:<?= $student->email ?>"><?= $student->email ?></a></p>
                </td>
                <td><?= $student->admission_no ?></td>
                <td>
                    <?php foreach ($subjects as $subject) {
                        echo "<li>" . $subject->subject_name . " ( " .$subject->subject_type." ) </li>";} ?>
                </td>
                <td>
                    <?php if($student->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span></td>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span></td>
                    <?php } ?>
                </td>
                <td>
                    <span><?= $user->fname.' '.$user->lname ?></span>
                    <p class="mt-2"><?= \Carbon\Carbon::parse($student->created_at)->format('d/m/Y | h:i A')?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Edit Action"  onclick="editStudent('<?=$student->id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteStudent('<?=$student->id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
