
<div class="table-responsive mailbox-messages">
    <table id="example2" class="table table-hover table-striped">
        <tbody>
            <?php if (!empty($data)) { foreach ($data as $item) { ?>

            <tr>
                <td>
                    <div class="mailbox-controls">
                        <div class="btn-group">
                            <button type="button" onclick="deleteEmail('<?=$item->id?>')" class="btn btn-default btn-sm">
                                <i class="far fa-trash-alt"></i>
                            </button>
                            <button type="button" onclick="reloadEmail('<?=$item->id?>')" class="btn btn-default btn-sm">
                            <i class="fas fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </td>
                <td class="mailbox-name">
                    <a href="javascript:" onclick="readMail(<?=$item->id?>)"><?=$item->subject?></a>
                </td>
                <td class="mailbox-subject">
                    <b><?= implode(' ', array_slice(explode(' ', strip_tags($item->message)), 0, 10)) . '...' ?></b>
                </td>
                <td>
                    <?php if($item->status==0){ ?>
                    <small class="btn btn-link p-2" onclick="readMail(<?=$item->id?>)">new</small>
                    <?php } ?>
                </td>
                <td class="mailbox-date"><?= \Carbon\Carbon::parse($item->created_at)->diffForHumans() ?></td>
            </tr>

            <?php }  } else { ?>
            <tr><td style="width: 100%">Message not available</td></tr>
            <?php } ?>

        </tbody>
    </table>
</div>

@include('dependences.datatable')
