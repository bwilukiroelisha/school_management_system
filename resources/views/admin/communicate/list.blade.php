

@extends('layouts.app')
@section('content')
@include('dependences.style')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-md-12 col-sm-12">
          <h1>{{ $data['header'] }}</h1>
        </div>
      </div>
    </div>
</section>


<section class="content">
    <div class="row">
      <div class="col-md-3">
        <button type="button" onclick="clear_input()"  data-toggle="modal" data-target="#modal-lg" title="Compose" class="btn btn-primary btn-block mb-3">Compose</button>
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Folders</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body p-0">
            <ul class="nav nav-pills flex-column">
              <li class="nav-item active">
                <a href="javascript:" class="nav-link">
                  <i class="fas fa-inbox"></i> Inbox
                  <span class="badge bg-primary float-right">{{ $msg->count() ? : '0' }}</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="javascript:" class="nav-link">
                  <i class="far fa-envelope"></i> Sent
                </a>
              </li>
              <li class="nav-item">
                <a href="javascript:" class="nav-link">
                  <i class="far fa-file-alt"></i> Drafts
                </a>
              </li>
              <li class="nav-item">
                <a href="javascript:" class="nav-link">
                  <i class="fas fa-filter"></i> Junk
                  <span class="badge bg-warning float-right">65</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="javascript:" class="nav-link">
                  <i class="far fa-trash-alt"></i> Trash
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Inbox</h3>
          </div>
          <div class="card-body p-0">

                <div class="alert alert-success alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
                </div>

                <div class="alert alert-danger alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
                </div>

                <div id="getView">
                    <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
                </div>

          </div>
        </div>
      </div>
    </div>
</section>




<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content card card-primary card-outline">
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <div class="">
                        <div class="card-header">
                          <h2 class="card-title">Compose New Message</h2>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Subject <span class="text-danger">*</span></label>
                                <input class="form-control" name="subject" id="subject" placeholder="Subject:" required>
                            </div>
                            <div class="form-group">
                                <label for="">User <span class="text-danger">*</span></label>
                                <select name="user" id="user" class="form-control select2" style="width: 100%;">
                                    <option value="">Search user</option>
                                    <?php foreach ($users as $user) {?>
                                    <option value="<?=$user->id?>"><?=$user->fname.' '.$user->mname.' '.$user->lname.' ( '.$user->role_name.' ) '?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Message To <span class="text-danger">*</span></label>
                                <div>
                                    <label for="" style="margin-right: 50px;">
                                        Teacher <input type="checkbox" name="message_to[]" id="" value="2" class="form-control" style="transform: scale(0.6);">
                                    </label>
                                    <label for="" style="margin-right: 50px;">
                                        Student <input type="checkbox" name="message_to[]" id="" value="3" class="form-control" style="transform: scale(0.6);">
                                    </label>
                                    <label for="" style="margin-right: 50px;">
                                        Parent <input type="checkbox" name="message_to[]" id="" value="4" class="form-control" style="transform: scale(0.6);">
                                    </label>
                                </div>
                                <small class="text-danger">Please select at least one recipient. Otherwise, the notice will not be visible to any recipients.</small>
                            </div>
                            <div class="form-group">
                                <label for="">Mesaage <span class="text-danger">*</span></label>
                                <textarea class="form-control summernote" id="message" name="message"  style="height: 300px" required> </textarea>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fas fa-paperclip"></i> Attachment
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                                <p class="help-block">PDF (Max. 32MB)</p>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-end"><hr>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" ><i class="fas fa-times"></i> DISCARD</button>
                            <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> SEND</button>
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-mail">
    <div class="modal-dialog modal-lg">
        <div class="modal-content card card-primary card-outline">
            <div class="modal-body">
                <div id="mail-details" class="card123">
                    <p>Loading...</p>
                </div>
                <span onclick="closeModel()" class="btn btn-sm btn-secondary float-right">CLOSE</span>
            </div>
        </div>
    </div>
</div>


@include('dependences.script')


<script>

$(document).ready(function () {
    getView();
    closeModel();
});

function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('admin/communicate/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}

function closeModel(){
    $('#modal-lg').modal('hide');
    $('#modal-mail').modal('hide');
}

function readMail(id){
    $('#modal-mail').modal('show');
    $.ajax({
        type: "GET",
        url: "/admin/communicate/read_mail/"+id,
        dataType: 'json',
        success: function (data) {

            var rowData=data.data;
            let formattedDate = moment(rowData.created_at).format('MMMM Do YYYY, h:mm A');
            let attachmentUrl = `/attachment/communicate/${rowData.attachment}`;

            let html = `
                <div class="card-body p-0">
                    <div class="mailbox-read-info">
                        <h5 class="fw-bold text-uppercase">${rowData.subject}</h5>
                    </div>
                    <span class="float-right">${formattedDate}</span>
                    <br>
                    <div class="mailbox-read-message">
                        <p>Hello <br><br><?=Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname?></p>

                        <p>${rowData.message}</p>

                        <p><br>Thanks,<br></p>
                    </div>
                </div>
                <br>
                <div class="card-footer bg-white">
                    <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                        <li>
                            <div class="mailbox-attachment-info">
                            ${rowData.attachment ? `<a href="${attachmentUrl}" class="btn btn-info" target="_blank">Download</a>` : '<p>No file attached</p>'}
                            </div>
                        </li>
                    </ul>
                </div>
                `;
                $('#mail-details').html(html);
                getView();

        }
    });

}

function reloadEmail(id){
    getView()
}

function deleteEmail(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
        return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/communicate/delete/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }
            }
    });
}

function editClass(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/class/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#name").val(rowData.name);
                $("#status1").val(rowData.status);
            }
    });
}

function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/communicate/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                closeModel();
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('.alert').fadeOut(7000);
                closeModel();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
