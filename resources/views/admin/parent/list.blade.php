
@extends('layouts.app')
@section('content')
@include('dependences.style')


@include('layouts.sub_header')


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">


            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>

            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>

            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>




<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="fname">First name <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" id="fname" name="fname" class="form-control" placeholder="Enter first name" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="mname">Middle name</label>
                            <div class="form-group">
                                <input type="text" id="mname" name="mname" class="form-control" placeholder="Enter middle name">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="lname">Last name <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" id="lname" name="lname" class="form-control" placeholder="Enter last name" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="phone">Phone <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="number" id="phone" name="phone" class="form-control" placeholder="Enter phone number" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="email">Email <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="email" id="email" name="email" class="form-control" placeholder="Enter email address">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="exampleInputFile">Image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image" placeholder="Image">
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="address">Current Address <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="current_address" id="current_address" placeholder="Tabata, Mwanza Tanzania" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="address">Permanent Address <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="permanent_address" id="permanent_address" placeholder="Ilemela, Mwanza Tanzania" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="work">Work <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="work" id="work" placeholder="Work or professional" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label class="form-label">Gender <span class="text-danger">*</span></label>
                            <select name="gender" id="gender" class="form-control  select2" style="width: 100%;">
                                <option value="" disable="disabled">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label class="form-label">Status <span class="text-danger">*</span></label>
                            <select name="status1" id="status1" class="form-control  select2" style="width: 100%;">
                                <option>Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('dependences.script')

<script>
$(document).ready(function () {
    getView();
    closeModel();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('admin/parent/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

function closeModel(){
    $('#modal-lg').modal('hide');
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}

function deleteParent(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
            return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/parent/delete/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }else{
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                }
            }
    });
}

function editParent(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/parent/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#fname").val(rowData.fname);
                $("#mname").val(rowData.mname);
                $("#lname").val(rowData.lname);
                $("#phone").val(rowData.phone);
                $("#email").val(rowData.email);
                $("#work").val(rowData.work);
                $("#current_address").val(rowData.current_address);
                $("#permanent_address").val(rowData.permanent_address);
                $("#gender").val(rowData.gender);
                $("#status1").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/parent/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('.alert').fadeOut(7000);
                closeModel();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
