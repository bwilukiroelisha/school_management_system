
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Profession</th>
                <th>Loaction</th>
                <th>Created By/On</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Profession</th>
                <th>Loaction</th>
                <th>Created By/On</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $user = App\Models\AdminModel::findAdmin($item->created_by); ?>

            <tr>
                <td><?=$n?></td>
                <td>
                    <div>
                        <?php if(!empty($item->image)){ ?>
                        <img src="{{ asset('upload/profile/'.$item->image) }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php }else{ ?>
                        <img src="{{ asset('assets/avatar.jpg') }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php } ?>
                        <small class="p-2 text-muted"><?= $item->gender ?></small>
                    </div>
                    <p class="c_name"><?= $item->fname.' '.$item->mname.' '.$item->lname ?></p>
                </td>
                <td>
                    <span><a href="tel:<?= $item->phone ?>" class="text-dark mb-2"><?= $item->phone ?></a></span>
                    <p class="mt-2"><a href="mailto:<?= $item->email ?>"><?= $item->email ?></a></p>
                </td>
                <td><?= $item->work ?></td>
                <td><?= $item->current_address ?></td>
                <td>
                    <span><?= $user->fname.' '.$user->lname ?></span>
                    <p class="mt-2"><?= \Carbon\Carbon::parse($item->created_at)->format('d/m/Y | h:i A')?></p>
                </td>
                <td>
                <?php if($item->status==0){ ?>
                <span class="badge badge-success p-2">Active</span>
                <?php }else{ ?>
                <span class="badge badge-warning p-2">Inactive</span>
                <?php } ?>
                <p class="mt-3"><a href="<?= url('admin/parent/my_student/'.Crypt::encrypt($item->id)) ?>" class="text-link mt-2">My Student</a></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Edit Action"  onclick="editParent('<?=$item->id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteParent('<?=$item->id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
