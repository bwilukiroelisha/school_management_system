
<div class="table-responsive">
    <table id="example3" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach ($subjects as $item) {?>
            <tr>
                <td><?=$n?></td>
                <td><?= $item->name ?></td>
                <td><?= $item->type ?></td>
                <td>
                    <?php if($item->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span>
                    <?php } ?>
                </td>
                <td>
                    <div class="btn-group">
                        {{-- <button title="Edit Action"  onclick="editTeacherSubject('<?=$item->id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button> --}}
                        <button title="Delete Action" onclick="deleteTeacherSubject('<?=$item->id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
