
@extends('layouts.app')
@section('content')
@include('dependences.style')

@include('layouts.sub_header')


<section class="content">
    <div class="card card-solid card-primary card-outline">
      <div class="card-body pb-0">
        <div class="row">
          <div class="col-sm-12 col-md-4 d-flex align-items-stretch flex-column">
            <div class="teacher_details">
                <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-1 h5 mb-4">
                    TEACHER
                </div>
                <input type="hidden" name="<?= $teacher->id ?>" id="teacher_id" value="<?= $teacher->id ?>">
                <div class="card-body pt-0">
                    <div class="row">
                    <div class="col-7">
                        <h2 class="lead mb-4"><b><?= $teacher->fname.' '.$teacher->mname.' '.$teacher->lname ?></b></h2>
                        <ul class="ml-4 mb-0 fa-ul text-muted">
                            <li class="small mb-2"><span class="fa-li"><i class="fas fa-building"></i></span>
                                <?= $teacher->work ?>
                            </li>
                            <li class="small mb-2"><span class="fa-li"><i class="fas fa-home"></i></span>
                                <?= $teacher->current_address ?>
                            </li>
                            <li class="small mb-2"><span class="fa-li"><i class="far fa-envelope"></i></span>
                                <a href="tel:<?= $teacher->email ?>" class="px-2"><?= $teacher->email ?></a>
                            </li>
                            <li class="small mb-2"><span class="fa-li"><i class="fas fa-phone"></i></span>
                                <a href="tel:<?= $teacher->phone ?>" class="px-2"><?= $teacher->phone ?></a>
                            </li>
                            <li class="small mb-2"><span class="fa-li"><i class="fas fa-map"></i></span>
                                <?= $teacher->permanent_address ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-5 text-center">
                        <?php if(!empty($teacher->image)){ ?>
                            <img src="{{ asset('upload/profile/'.$teacher->image) }}" alt="Profile" class="img-circle img-fluid">
                            <?php }else{ ?>
                            <img src="{{ asset('assets/avatar.jpg') }}" alt="Profile" class="img-circle img-fluid">
                        <?php } ?>
                    </div>
                    </div>
                </div>
                </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-8 d-flex align-items-stretch flex-column">
            <div class="card-header text-muted border-bottom-1 h5 mb-2">
                MY SUBJECT
            </div>
            <div class="card-body">
                <div class="alert alert-success alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
                </div>

                <div class="alert alert-danger alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
                </div>

                <div id="getView">
                    <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
</section>




<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">

                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="hidden" class="form-control" id="<?= $teacher->id ?>" name="teacher_id" value="<?= $teacher->id ?>" autocomplete="off">
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Assign Subjects</label>
                                <select class="duallistbox" multiple="multiple" name="subject_id[]" id="subject_id" required >
                                    <option>~Select~</option>
                                    <?php foreach ($subject as $sub) { ?>
                                    <option value="<?= $sub->id ?>"><?= $sub->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@include('dependences.script')

<script>
$(document).ready(function () {
    getView();
    $("#searchText").on('keyup',function(){
        searchText()
    })

});


function addText(id,fname,mname,lname,admission_no){
    $("#student_id").val(id)
    $("#searchText").val(fname+' '+mname+' '+lname+' '+admission_no)
    $("#searchResult").html("")
}

function searchText(){
    var searchText=$("#searchText").val();
    $("#student_id").val("")
    jQuery.ajax({
            type: "GET",
            url: "/admin/parent/searchStudent/"+searchText,
            dataType:'html',
            success: function(data) {
                $("#searchResult").html(data)
            }

    });
}


function getView() {
    var teacher_id = $("#teacher_id").val();
    jQuery.ajax({
        type: "GET",
        url: "/admin/teacher/my_subject_view/"+teacher_id,
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

function closeModel(){
    $('#modal-lg').modal('hide');
    clear_input();
}


function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("");
    $("#teacher_id").val("");
    $("#subject_id").val("");
    getView()
}

function deleteTeacherSubject(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
            return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/teacher/deleteTeacherSubject/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }else{
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                }
            }
    });
}

function editParent(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/parent/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#fname").val(rowData.fname);
                $("#mname").val(rowData.mname);
                $("#lname").val(rowData.lname);
                $("#phone").val(rowData.phone);
                $("#email").val(rowData.email);
                $("#address").val(rowData.address);
                $("#gender").val(rowData.gender);
                $("#status1").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/teacher/addMySubject')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
                getView();
            if (data.status == 200) {
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                clear_input();
            }else{
                $('.alert-danger').show();
                $('#error').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
            }
        }
    });
}
</script>
@endsection
