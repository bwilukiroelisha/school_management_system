
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Day</th>
                <th>Subject</th>
                <th>Class</th>
                <th>Start Time</th>
                <th>Finish Time</th>
                <th>Teacher</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Day</th>
                <th>Subject</th>
                <th>Class</th>
                <th>Start Time</th>
                <th>Finish Time</th>
                <th>Teacher</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $user = App\Models\AdminModel::findAdmin($item->created_by); ?>
            <tr>
                <td><?= $n ?></td>
                <td><?= $item->day ?></td>
                <td><?= $item->subject ?></td>
                <td><?= $item->class ?></td>
                <td><?= $item->start_time ?></td>
                <td><?= $item->finish_time ?></td>
                <td>
                    {{-- <span><a href="tel:<?= $teacher->phone ?>" class="text-dark mb-2"><?= $teacher->phone ?></a></span>
                    <p class="mt-2"><a href="mailto:<?= $teacher->email ?>"><?= $teacher->email ?></a></p> --}}
                </td>
                <td>
                <?php if($item->status==0){ ?>
                <span class="badge badge-success p-2">Active</span></td>
                <?php }else{ ?>
                <span class="badge badge-warning p-2">Inactive</span></td>
                <?php } ?>
                </td>
                <td>
                    <span><?= $user->fname.' '.$user->mname.' '.$user->lname ?></span>
                    <p class="mt-2"><?= \Carbon\Carbon::parse($item->created_at)->format('d/m/Y | h:i A')?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Edit Action"  onclick="editTimetable('<?=$item->id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteTimetable('<?=$item->id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')


