

@extends('layouts.app')
@section('content')
@include('dependences.style')


@include('layouts.sub_header')


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">

            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>

            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>

            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>




<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-6 col-sm-12 form-group">
                            <label class="form-label">Day <span class="text-danger">*</span></label>
                            <select name="day_id" id="day_id" class="form-control  select2" style="width: 100%;" required>
                                <option value="" selected="selected">Select</option>
                                <?php foreach ($days as $item) { ?>
                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="name">Subject <span class="text-danger">*</span></label>
                            <select name="subject_id" id="subject_id" class="form-control  select2" style="width: 100%;" required>
                                <option value="" selected="selected">Select</option>
                                <?php foreach ($subject as $item) { ?>
                                <option value="<?= $item->id ?>"><?= $item->name.' ('.$item->type.')' ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="name">Class <span class="text-danger">*</span></label>
                            <select name="class_id" id="class_id" class="form-control  select2" style="width: 100%;" required>
                                <option value="" selected="selected">Select</option>
                                <?php foreach ($class as $item) { ?>
                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="name">Start <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="time" id="start_time" name="start_time" class="form-control" placeholder="Enter start time" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="name">End <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="time" id="finish_time" name="finish_time" class="form-control" placeholder="Enter end time" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label class="form-label">Status <span class="text-danger">*</span></label>
                            <select name="status1" id="status1" class="form-control  select2" style="width: 100%;" required>
                                <option>Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('dependences.script')

<script>
$(document).ready(function () {
    getView();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('admin/timetable/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}

function closeModel(){
    $('#modal-lg').modal('hide');
}

function deleteTimetable(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
        return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/timetable/delete/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }
            }
    });
}

function editTimetable(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/timetable/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#day_id").val(rowData.day_id);
                $("#subject_id").val(rowData.subject_id);
                $("#class_id").val(rowData.class_id);
                $("#start_time").val(rowData.start_time);
                $("#finish_time").val(rowData.finish_time);
                $("#status1").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/timetable/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
