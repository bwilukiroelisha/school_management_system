
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Noticed</th>
                <th>Published</th>
                <th>Attachment</th>
                <th>Message</th>
                <th>Recipients</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $notice = $item->first();
                $user = App\Models\AdminModel::findAdmin($notice->created_by); ?>
            <tr>
                <td><?=$n?></td>
                <td><?= $notice->title ?></td>
                <td><?= $notice->notice_date ?></td>
                <td><?= $notice->published_date ?></td>
                <td>
                    @if (!empty($notice->attachment))
                    <a href="<?= url('/upload/notices/'.$notice->attachment) ?>" class="btn btn-info" target="_blank">Download</a>
                    @else
                    <span>No file</span>
                    @endif
                </td>
                <td><?= implode(' ', array_slice(explode(' ', strip_tags($notice->message)), 0, 10)) . '...' ?></td>
                <td>
                    <ul>
                        @foreach($item as $record)
                            <li><small>{{ $record->role_name }}</small></li>
                        @endforeach
                    </ul>
                </td>
                <td>
                <?php if($notice->status==0){ ?>
                <span class="badge badge-success p-2">Active</span></td>
                <?php }else{ ?>
                <span class="badge badge-warning p-2">Inactive</span></td>
                <?php } ?>
                </td>
                <td>
                    <span><?= $user->fname.' '.$user->mname.' '.$user->lname ?></span>
                    <p class="mt-2"><?= \Carbon\Carbon::parse($notice->created_at)->format('d/m/Y | h:i A')?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Edit Action"  onclick="editNotice('<?=$notice->notice_id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteNotice('<?=$notice->notice_id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Noticed</th>
                <th>Published</th>
                <th>Attachment</th>
                <th>Message</th>
                <th>Recipients</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
</div>

@include('dependences.datatable')

