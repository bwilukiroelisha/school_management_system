


@extends('layouts.app')
@section('content')
@include('dependences.style')


@include('layouts.sub_header')


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">

            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>

            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>

            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>


<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="">Title <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" id="title" name="title" class="form-control" placeholder="Enter notice title" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="">Notice Date</label>
                            <div class="form-group">
                                <input type="date" id="notice_date" name="notice_date" class="form-control" placeholder="Enter notice date" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="">Published Date<span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="date" id="published_date" name="published_date" class="form-control" placeholder="Enter published date" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="">Message To <span class="text-danger">*</span></label>
                            <div>
                                <label for="" style="margin-right: 50px;">
                                    Teacher <input type="checkbox" name="message_to[]" id="" value="2" class="form-control" style="transform: scale(0.6);">
                                </label>
                                <label for="" style="margin-right: 50px;">
                                    Student <input type="checkbox" name="message_to[]" id="" value="3" class="form-control" style="transform: scale(0.6);">
                                </label>
                                <label for="" style="margin-right: 50px;">
                                    Parent <input type="checkbox" name="message_to[]" id="" value="4" class="form-control" style="transform: scale(0.6);">
                                </label>
                            </div>
                            <small class="text-danger">Please select at least one recipient. Otherwise, the notice will not be visible to any recipients.</small>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="">Mesaage <span class="text-danger">*</span></label>
                            <textarea class="form-control summernote" id="message" name="message"  style="height: 200px" required> </textarea>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="">Attachment (Optional)</label><br>
                            <div class="btn btn-default btn-file">
                                <i class="fas fa-paperclip"></i> Attachment
                                <input type="file" name="attachment" id="attachment">
                            </div>
                            <p class="help-block">Max. 32MB</p>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label class="form-label">Status <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required>
                                <option selected="selected">Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@include('dependences.script')

<script>
$(document).ready(function () {

    var today = new Date().toISOString().split('T')[0];
    $('#notice_date').attr('min', today);
    $('#published_date').attr('min', today);


    getView();
});




function getView() {
jQuery.ajax({
    type: "GET",
    url: "{{ url('admin/notice_board/list_view')}}",
    dataType: 'html',
    cache: false,
    success: function (data) {
        $("#getView").html(data)
    }
});
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}

function closeModel(){
    $('#modal-lg').modal('hide');
}

function deleteNotice(id){
    var conf = confirm("Are you sure you want to delete this Admin ?");
    if (!conf) {
            return;
    }

    jQuery.ajax({
        type: "GET",
        url: "/admin/notice_board/delete/"+id,
        dataType: 'json',
        success: function (data) {
            if (data.status == 200) {
                $('.alert-danger').show();
                $('#error').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }
        }
    });
}

function editNotice(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("");

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/notice_board/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#title").val(rowData.title);
                $("#notice_date").val(rowData.notice_date);
                $("#published_date").val(rowData.published_date);
                $("#message_to").val(rowData.message_to);
                $("#message").summernote('code', rowData.message);
                $("#attachment").val(rowData.attachment);
                $("#status").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/notice_board/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('.alert').fadeOut(7000);
                closeModel();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
