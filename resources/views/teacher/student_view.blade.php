
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Admission No./Date</th>
                <th>DoB</th>
                <th>Religion</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Admission No./Date</th>
                <th>DoB</th>
                <th>Religion</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) { ?>
            <tr>
                <td><?=$n?></td>
                <td>
                    <div>
                        <?php if(!empty($item->image)){ ?>
                        <img src="{{ asset('upload/profile/'.$item->image) }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php }else{ ?>
                        <img src="{{ asset('assets/avatar.jpg') }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php } ?>
                        <small class="p-2 text-muted"><?= $item->gender ?></small>
                    </div>
                    <p class="c_name"><?= $item->fname.' '.$item->mname.' '.$item->lname ?></p>
                </td>
                <td>
                    <span><a href="tel:<?= $item->phone ?>" class="text-dark mb-2"><?= $item->phone ?></a></span>
                    <p class="mt-2"><a href="mailto:<?= $item->email ?>"><?= $item->email ?></a></p>
                </td>
                <td>
                    <span><?= $item->admission_no ?></span>
                    <p class="mt-2 text-primary"><?= $item->admission_date ?></p>
                </td>
                <td><?= $item->dob ?></td>
                <td><?= $item->religion ?></td>
                <td>
                    <?php if($item->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span></td>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span></td>
                    <?php } ?>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Update Action" onclick="updateStudent('<?=$item->id?>')"  class="btn btn-outline-secondary"><i class="fas fa-edit"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
