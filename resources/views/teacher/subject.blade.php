

@extends('layouts.app')
@section('content')
@include('dependences.style')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-md-12 col-sm-12">
          <h1>{{ $data['header'] }}</h1>
        </div>
      </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>
            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>
            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>


<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal"  onclick="closeModel()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Class</th>
                    </tr>
                </thead>
                <tbody id="dataTable">
                </tbody>
            </table>

        </div>
        <div class="modal-footer justify-content-end">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
        </div>
      </div>
    </div>
</div>

<script>
$(document).ready(function () {
    getView();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('teacher/subject/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}


function closeModel(){
    $('#modal-lg').modal('hide');
}


function viewTimetable(id,name,type)
{
    $('#modal-lg').modal('show');
    $('.modal-title').html(name+' ( '+type+' )').show();

    jQuery.ajax({
        type: "GET",
        url: "/teacher/view_timetable/"+id,
        dataType: 'json',
        success: function (data) {
            $("#hidden_id").val(data.id)
            $('#dataTable').html(data.html);
            console.log(data.html);

        }
    });

}


function deleteStudent(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
            return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/admin/student/delete/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }
            }
    });
}

function editStudent(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("Update");
    $('#modal-lg').modal('show');

    jQuery.ajax({
            type: "GET",
            url: "/admin/student/edit/"+id,
            dataType: 'json',
            success: function (data) {
                $("#hidden_id").val(data.id)

                var rowData=data.data;

                $("#fname").val(rowData.fname);
                $("#mname").val(rowData.mname);
                $("#lname").val(rowData.lname);
                $("#phone").val(rowData.phone);
                $("#email").val(rowData.email);
                $("#dob").val(rowData.dob);
                $("#admission_no").val(rowData.admission_no);
                $("#admission_date").val(rowData.admission_date);
                $("#permanent_address").val(rowData.permanent_address);
                $("#class_id").val(rowData.class_id);
                $("#religion").val(rowData.religion);
                $("#gender").val(rowData.gender);
                $("#status1").val(rowData.status);
            }
    });
}


function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('admin/student/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('#error').text(data.message);
            }
        }
    });
}
</script>
@endsection
