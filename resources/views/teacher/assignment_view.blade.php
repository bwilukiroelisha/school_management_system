
<div class="table-responsive">
    <table id="example3" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Subject</th>
                <th>File</th>
                <th>Assignment</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $subject = App\Models\SubjectModel::findSubject($item->subject_id); ?>
            <tr>
                <td><?= $n; ?></td>
                <td><?= $subject->name.' ( '.$subject->type.' )'; ?></td>
                <td>
                    @if ($item->file)
                    <a href="{{ asset('attachment/assignments/'.$item->file) }}" target="_blank" class="btn btn-info">Download</a>
                    @else
                    No file
                    @endif
                </td>
                <td><?= $item->template ?></td>
                <td><?= $item->status ? '<span class="badge badge-warning p-2">Inactive</span>' : '<span class="badge badge-success p-2">Active</span>' ?></td>
                <td>
                    <div class="btn-group">
                        <button title="Update Action" onclick="editAssignment('<?=$item->id?>')"  class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteAssignment('<?=$item->id?>')"  class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php $n++; } ?>

        </tbody>
    </table>
</div>


@include('dependences.datatable')
