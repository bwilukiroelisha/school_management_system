
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Subject</th>
                <th>Exam Date</th>
                <th>Start Time</th>
                <th>Finish Time</th>
                <th>Class/Room</th>
                <th>Full Marks</th>
                <th>Pass Marks</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Subject</th>
                <th>Exam Date</th>
                <th>Start Time</th>
                <th>Finish Time</th>
                <th>Class/Room</th>
                <th>Full Marks</th>
                <th>Pass Marks</th>
                <th>Status</th>
                <th>Created By/On</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $user = App\Models\AdminModel::findAdmin($item->created_by); ?>
            <tr>
                <td><?= $n ?></td>
                <td><?= $item->subject ?></td>
                <td><?= \Carbon\Carbon::parse($item->date)->format('d/m/Y') ?></td>
                <td><?= $item->start_time ?></td>
                <td><?= $item->finish_time ?></td>
                <td><?= $item->class ?></td>
                <td><?= $item->full_marks ?></td>
                <td><?= $item->grade ?></td>
                <td>
                    <?php if($item->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span></td>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span></td>
                    <?php } ?>
                </td>
                <td>
                    <span><?= $user->fname.' '.$user->mname.' '.$user->lname ?></span>
                    <p class="mt-2"><?= \Carbon\Carbon::parse($item->created_at)->format('d/m/Y | h:i A')?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Edit Action"  onclick="editExam('<?=$item->id?>')" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                        <button title="Delete Action" onclick="deleteExam('<?=$item->id?>')"  class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')


