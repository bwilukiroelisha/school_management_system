
@extends('layouts.app')
@section('content')
@include('dependences.style')


@include('layouts.sub_header')


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>
            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>
            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>



<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Assignment</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">
                <form id="form" onsubmit="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="">Subject <span class="text-danger">*</span></label>
                            <select name="subject_id" id="subject_id" class="form-control  select2" style="width: 100%;" required>
                                <option>~Choose subject~</option>
                                <?php foreach ($teacher_subjects as $subject) { ?>
                                <option value="<?=$subject->id?>"><?= $subject->name.' ( '.$subject->type.' )' ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <label for="exampleInputFile">File </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file" name="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label for="name">Template <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <textarea name="template" id="template" rows="50" class="form-control summernote"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <label class="form-label">Status <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control  select2" style="width: 100%;" required>
                                <option>Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CANCEL</button>
                        <button type="submit" class="btn btn-primary" id="submitBtn">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@include('dependences.script')

<script>
$(document).ready(function () {
    getView();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "/teacher/assignment/list_view",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}


function closeModel(){
    clear_input();
    $('#modal-lg').modal('hide');
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView();
}

function deleteAssignment(id){
    var conf = confirm("Are you sure you want to delete this record ?");
    if (!conf) {
        return;
    }

    jQuery.ajax({
        type: "GET",
        url: "/teacher/assignment/delete/"+id,
        dataType: 'json',
        success: function (data) {
            if (data.status == 200) {
                $('.alert-danger').show();
                $('#error').text(data.message);
                $('.alert').fadeOut(7000);
                closeModel();
                clear_input();
                getView();
            }
        }
    });
}

function editAssignment(id){
    document.getElementById('form').reset();
    $("#hidden_id").val("")

    $("#submitBtn").html("UPDATE");
    $('#modal-lg').modal('show');

    jQuery.ajax({
        type: "GET",
        url: "/teacher/assignment/edit/"+id,
        dataType: 'json',
        success: function (data) {
            $("#hidden_id").val(data.id)

            var rowData=data.data;

            $("#subject_id").val(rowData.subject_id);
            $("#file").val(rowData.file);
            $("#template").val(rowData.template);
            $("#status").val(rowData.status);
        }
    });
}



function save(e) {
    e.preventDefault();

    var form = document.getElementById('form');
    var formData = new FormData(form);

    jQuery.ajax({
        type: "POST",
        url: "{{ url('teacher/assignment/add')}}",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            if (data.status == 200) {
                closeModel();
                $('.alert-success').show();
                $('#success').text(data.message);
                $('.alert').fadeOut(7000);
                clear_input();
                getView();
            }else{
                $('.alert-danger').show();
                $('.alert').fadeOut(7000);
                closeModel();
                $('#error').text(data.message);
            }
        }
    });
}

</script>
@endsection
