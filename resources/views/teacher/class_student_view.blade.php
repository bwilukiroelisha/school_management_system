
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Student</th>
                <th>Works</th>
                <th>Total</th>
                <th>Results</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Student</th>
                <th>Works</th>
                <th>Total</th>
                <th>Results</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $student = App\Models\StudentModel::findStudent($item->student_id); ?>
            <tr>
                <td><?=$n?></td>
                <td>
                    <div>
                        <?php if(!empty($student->image)){ ?>
                        <img src="{{ asset('upload/profile/'.$student->image) }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php }else{ ?>
                        <img src="{{ asset('assets/avatar.jpg') }}" class="rounded-circle avatar" alt="profile" style="width:50px;height:50px;border-radius:50px;">
                        <?php } ?>
                        <small class="p-2 text-muted"><?= $student->gender ?></small>
                    </div>
                    <p class="c_name"><?= $student->fname.' '.$student->mname.' '.$student->lname ?></p>
                </td>
                <td>
                    <small><strong>Class : </strong> <?=$item->class_work?></small><br>
                    <small><strong>Test : </strong> <?=$item->test_work?></small><br>
                    <small><strong>Assignment : </strong> <?=$item->assignment_work?></small><br>
                    <small><strong>Examination : </strong> <?=$item->exam?></small>
                </td>
                <td><strong><?=$item->total?></strong></td>
                <td>
                    <small>Grade : <strong><?=$item->grade?></strong> </small>
                    <p><small class="badge badge-pill badge-info"><?=$item->remark?></small></p>
                </td>
                <td>
                    <?php if($item->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span>
                    <?php } ?>
                </td>
                <td>
                    <div class="btn-group">
                        <button title="Add Action" onclick="addMarks(<?=$item->subject_id?>,<?=$item->student_id?>)"  class="btn btn-outline-info"><i class="fas fa-plus"></i>Add</button>
                        <button title="Update Action" onclick="updateMarks(<?=$item->id?>,<?=$item->subject_id?>,<?=$item->student_id?>)"  class="btn btn-outline-warning"><i class="fas fa-edit"></i>Update</button>
                    </div>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
