

@extends('layouts.app')
@section('content')
@include('dependences.style')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-md-12 col-sm-12">
          <h1>{{ $data['header'] }}</h1>
        </div>
      </div>
    </div>
</section>


<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i><span id="success"> Alert!</span></h5>
            </div>
            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i><span id="error"> Alert!</span></h5>
            </div>


            <input type="hidden" name="<?= $subject->id ?>" id="subject_id" value="<?= $subject->id ?>">


            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>




<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registration</h4>
                <strong class="text-danger float-right">Required *</strong>
            </div>
            <div class="modal-body">

                <form id="form" onsubmit1234="save(event)" enctype="form-data/multipart">
                    @csrf
                    <input type="hidden" class="form-control" id="hidden_id" name="hidden_id" >
                    <div class="row">
                        <div class="col-md-4 col-sm-12 form-group">
                            <label for="">Class Work <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="number" id="class" name="class" class="form-control" placeholder="Enter class work" >
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 form-group">
                            <label for="">Assignment Work <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="number" id="assignment" name="assignment" class="form-control" placeholder="Enter assignment" >
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 form-group">
                            <label for="">Test Work <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="number" id="test" name="test" class="form-control" placeholder="Enter test work" >
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 form-group">
                            <label for="">Exam <span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="number" id="exam" name="exam" class="form-control" placeholder="Enter exam work" >
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 form-group">
                            <label class="">Status <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control  select2" style="width: 100%;" required>
                                <option>Select</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end"><hr>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModel()" >CLOSE</button>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<script>
$(document).ready(function () {
    getView();
});

function getView() {
    var subject_id = $("#subject_id").val();
    jQuery.ajax({
        type: "GET",
        url: "/teacher/class_student/list_view/"+subject_id,
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}


function closeModel(){
    $('#modal-lg').modal('hide');
}

function clear_input() {
    document.getElementById('form').reset();
    $("#hidden_id").val("")
    getView()
}


function addMarks(subject_id,student_id){
    $('#modal-lg').modal('show');
    $('.modal-title').html('Add Marks').show();
    clear_input();

    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

    $('form#form').submit(function (e) {
        e.preventDefault();

        var form = document.getElementById('form');
        var formData = new FormData(form);
        var subject_id = $("#subject_id").val();

        jQuery.ajax({
            type: "POST",
            url: "/teacher/marks/add/"+subject_id+"/"+student_id,
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-success').show();
                    $('#success').text(data.message);
                    $('.alert').fadeOut(7000);
                    closeModel();
                    clear_input();
                    getView();
                }else{
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                }
            }
        });
    });
}


function updateMarks(id,subject_id,student_id){
    $('#modal-lg').modal('show');
    $('.modal-title').html('Update Marks').show();
    clear_input();
    $("#hidden_id").val("")

    jQuery.ajax({
        type: "GET",
        url: "/teacher/marks/edit/"+id+"/"+subject_id+"/"+student_id,
        dataType: 'json',
        success: function (data) {
            $("#hidden_id").val(data.id)

            var rowData=data.data;

            $("#class").val(rowData.class_work);
            $("#assignment").val(rowData.assignment_work);
            $("#test").val(rowData.test_work);
            $("#exam").val(rowData.exam);
            $("#status").val(rowData.status);
        }
    });
}
function updateStudent(id){
    var conf = confirm("Are you sure you want to update this record ?");
    if (!conf) {
            return;
    }

    jQuery.ajax({
            type: "GET",
            url: "/teacher/student/update/"+id,
            dataType: 'json',
            success: function (data) {
                if (data.status == 200) {
                    $('.alert-success').show();
                    $('#success').text(data.message);
                    $('.alert').fadeOut(7000);
                    getView();
                }else{
                    $('.alert-danger').show();
                    $('#error').text(data.message);
                    $('.alert').fadeOut(7000);
                    getView();
                }
            }
    });
}


</script>
@endsection
