@extends('layouts.app')
@section('content')

<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">{{ $data['header'] }}</h1>
        </div>
    </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
            <a href="{{url('admin/student/list')}}">
                <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $student->count() ? : '0' }}</h3>
                    <p>Student</p>
                </div>
                <div class="icon">
                    <i class="fas fa-book-reader"></i>
                </div>
                </div>
            </a>
            </div>
            <div class="col-lg-3 col-6">
            <a href="{{url('admin/teacher/list')}}">
                <div class="small-box bg-success">
                <div class="inner">
                    <h3> {{ $teacher->count() ? : '0' }} </h3>
                    <p>Teacher</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user-plus"></i>
                </div>
                </div>
            </a>
            </div>

            <div class="col-lg-3 col-6">
            <a href="{{url('admin/subject/list')}}">
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $subject->count() ? : '0' }}</h3>
                    <p>Subject</p>
                </div>
                <div class="icon">
                    <i class="fa fa-slack"></i>
                </div>
                </div>
            </a>
            </div>

            <div class="col-lg-3 col-6">
            <a href="{{url('admin/class/list')}}">
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{ $class->count() ? : '0' }}</h3>
                        <p>Class</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-columns"></i>
                    </div>
                </div>
            </a>
            </div>
        </div>

        <hr><br>

        <div class="row">
            <section class="col-lg-7 connectedSortable">

            </section>

            <section class="col-lg-5 connectedSortable">
                <div class="card bg-gradient-success">
                    <div class="card-header border-0">

                    <h3 class="card-title">
                        <i class="far fa-calendar-alt"></i>
                        Calendar
                    </h3>
                    <div class="card-tools">
                        <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a href="#" class="dropdown-item">Add new event</a>
                            <a href="#" class="dropdown-item">Clear events</a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">View calendar</a>
                        </div>
                        </div>
                        <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                        </button>
                    </div>
                    </div>
                    <div class="card-body pt-0">
                    <div id="calendar" style="width: 100%"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>


@endsection
