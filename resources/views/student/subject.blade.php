

@extends('layouts.app')
@section('content')
@include('dependences.style')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-md-12 col-sm-12">
          <h1>{{ $data['header'] }}</h1>
        </div>
      </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
      <div class="card card-primary card-outline">
        <div class="card-body">
            <div id="getView">
                <img src="{{ asset('assets/plugins/loader.svg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
</section>

<script>
$(document).ready(function () {
    getView();
});


function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('student/subject/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

</script>
@endsection
