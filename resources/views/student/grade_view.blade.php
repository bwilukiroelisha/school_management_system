
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>From (%)</th>
                <th>To (%)</th>
                <th>Remark</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach ($data as $item) {?>
            <tr>
                <td><?=$n?></td>
                <td><?= $item->name ?></td>
                <td><?= $item->from ?></td>
                <td><?= $item->to ?></td>
                <td><?= $item->remark ?></td>
                <td>
                <?php if($item->status==0){ ?>
                <span class="badge badge-success p-2">Active</span></td>
                <?php }else{ ?>
                <span class="badge badge-warning p-2">Inactive</span></td>
                <?php } ?>
                </td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>From (%)</th>
                <th>To (%)</th>
                <th>Remark</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>
</div>

@include('dependences.datatable')

