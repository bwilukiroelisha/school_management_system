
<div class="table-responsive">
    <table id="example3" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Subject</th>
                <th>Type</th>
                <th>ClassWork</th>
                <th>Assignment</th>
                <th>TestWork</th>
                <th>Examination</th>
                <th>Total</th>
                <th>Grade</th>
                <th>Remark</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $subject = App\Models\SubjectModel::findSubject($item->subject_id); ?>
            <tr>
                <td><?=$n?></td>
                <td><?=$subject->name?></td>
                <td><?=$subject->type?></td>
                <td><?=$item->class_work?></td>
                <td><?=$item->assignment_work?></td>
                <td><?=$item->test_work?></td>
                <td><?=$item->exam?></td>
                <td><?=$item->total?></td>
                <td><?=$item->grade?></td>
                <td><?=$item->remark?></td>
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
