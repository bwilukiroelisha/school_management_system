
<div class="table-responsive">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Teacher</th>
                <th>Status</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Teacher</th>
                <th>Status</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $n=1; foreach ($data as $item) {
                $teacher = App\Models\TeacherModel::findTeacher($item->teacher_id); ?>
            <tr>
                <td><?= $n ?></td>
                <td><?= $item->name ?></td>
                <td><?= $item->type ?></td>
                <td><?= $teacher->fname.' '.$teacher->mname.' '.$teacher->lname ?></td>
                <td>
                    <?php if($item->status==0){ ?>
                    <span class="badge badge-success p-2">Active</span></td>
                    <?php }else{ ?>
                    <span class="badge badge-warning p-2">Inactive</span></td>
                    <?php } ?>
                </td>
                {{-- <td><button type="button" class="btn btn-sm btn-info px-3" onclick="viewTimetable('<?=$item->id?>','<?=$item->name?>','<?=$item->type?>')">View</button></td>
                <td><a href="<?= url('teacher/class_student/'.$item->id)?>" class="btn btn-sm btn-secondary px-3" >View</button></td> --}}
            </tr>
            <?php  $n++; } ?>
        </tbody>
    </table>
</div>


@include('dependences.datatable')
