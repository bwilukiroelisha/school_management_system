
@extends('layouts.app')
@section('content')
@include('dependences.style')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-md-12 col-sm-12">
          <h1>{{ $data['header'] }}</h1>
        </div>
      </div>
    </div>
</section>


<section class="content" id="printResult">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="invoice p-4 mb-3  card card-info card-outline">
            <div class="row">
              <div class="col-12 mb-4" style="border-bottom: 2px solid #000;">
                <h4 class="my-2">
                    <span class="text-center"><i class="fas fa-globe mr-4"></i> <strong><?=strtoupper($data['title'])?></strong></span>
                    <small class="float-right">Date: <small><?= date('d/m/Y | h:m A')?></small></small>
                </h4>
              </div>
            </div>
            <div class="row invoice-info mb-3">
                <div class="col-md-9 col-sm-12">
                    <div class="row mb-3">
                        <div class="col-md-3 col-sm-3 text-right1"><strong class="text-uppercase">Full Name : </strong></div>
                        <div class="col-md-8 col-sm-8 text-ce" style="border-bottom: 1px dotted grey;"><?= strtoupper(Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname)?></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-3 col-sm-3 text-right1"><strong class="text-uppercase">Admission Number : </strong></div>
                        <div class="col-md-8 col-sm-8" style="border-bottom: 1px dotted grey;"><?= Auth::user()->admission_no?></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-3 col-sm-3 text-right1"><strong class="text-uppercase">Email Address: </strong></div>
                        <div class="col-md-8 col-sm-8" style="border-bottom: 1px dotted grey;"><?= Auth::user()->email?></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-3 col-sm-3 text-right1"><strong class="text-uppercase">Phone Number: </strong></div>
                        <div class="col-md-8 col-sm-8" style="border-bottom: 1px dotted grey;"><?= Auth::user()->phone?></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="profile justify-content-end">

                        <?php if(!empty(Auth::user()->image)){ ?>
                        <img src="{{ asset('upload/profile/'.Auth::user()->image) }}" class="rounded-circle avatar float-right" alt="profile" style="width:150px;height:auto;border-radius:50px;">
                        <?php }else{ ?>
                        <img src="{{ asset('assets/avatar.jpg') }}" class="rounded-circle avatar float-right" alt="profile" style="width:150px;height:auto;border-radius:50px;">
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Subject</th>
                            <th>Type</th>
                            <th>Marks</th>
                            <th>Grade</th>
                            <th>Remark</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $n=1; foreach ($results as $result) {
                                $subject = App\Models\SubjectModel::findSubject($result->subject_id);?>
                            <tr>
                                <td><?=$n?></td>
                                <td><?=$subject->name?></td>
                                <td><?=$subject->type?></td>
                                <td><?=$result->total?></td>
                                <td><?=$result->grade?></td>
                                <td><?=$result->remark?></td>
                            </tr>
                            <?php $n++; } ?>
                            <tr>
                                <td colspan="2" class="text-right">
                                    <strong>SCORE : </strong>
                                </td>
                                <td colspan="4" class="text-right">
                                    <strong>RESULTS : </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
              <div class="col-10 justify-content-center">
                <p class="lead">Remarks:</p>

                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                  plugg
                  dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
              </div>
            </div>

            <div class="row no-print">
              <div class="col-12">
                <button type="button" onclick="printPage()" class="btn btn-primary float-right" style="margin-right: 5px;">
                  <i class="fas fa-download"></i> Generate PDF
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>


<script src="https://cdn.rawgit.com/DoersGuild/jQuery.print/master/jQuery.print.js"></script>

<script>
$(document).ready(function () {
    getView();
});


function printPage() {
    // $("#printResult").print();
    window.print();
}

function getView() {
    jQuery.ajax({
        type: "GET",
        url: "{{ url('student/results/list_view')}}",
        dataType: 'html',
        cache: false,
        success: function (data) {
            $("#getView").html(data)
        }
    });
}

</script>
@endsection
