<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background:#000000;">
    <!-- Brand Logo -->
    <a href="javascript:" class="brand-link" style="font-size:23px;text-align:center;font-weight:900!important;">
        <span class="brand-text font-weight-light">
            {{ !empty(strtoupper($data['title'])) ? strtoupper($data['title']) : 'School Records'}}
        </span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('assets/dist/img/avatar.png')}}" class="img-circle elevation-2" alt="Profile">
        </div>
        <div class="info">
          <a href="javascript:" class="d-block" title="My Name">{{ Str::ucfirst(Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname) }}</a>
          <small  class="text-muted text-center" id="user_role" title="My Role"></small>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        {{-- ADMIN --}}
        @if (Auth::user()->role == 1)

        <li class="nav-item">
            <a href="{{ url('admin/dashboard')}}" class="nav-link @if (Request::segment(2) == 'dashboard' ) active @endif ">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/admin/list') }}" class="nav-link @if (Request::segment(2) == 'admin' ) active @endif ">
                <i class="nav-icon fas fa-user"></i>
                <p>Admin</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/student/list') }}" class="nav-link @if (Request::segment(2) == 'student' ) active @endif ">
                <i class="nav-icon fas fa-users"></i>
                <p>Student</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/teacher/list') }}" class="nav-link @if (Request::segment(2) == 'teacher' ) active @endif ">
                <i class="nav-icon fas fa-users"></i>
                <p>Teacher</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/parent/list') }}" class="nav-link @if (Request::segment(2) == 'parent' ) active @endif ">
                <i class="nav-icon fas fa-users"></i>
                <p>Parent</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/student_subject/list') }}" class="nav-link @if (Request::segment(2) == 'student_subject' ) active @endif ">
                <i class="nav-icon fas fa-tree"></i>
                <p>Student Subject</p>
            </a>
        </li>
        <li class="nav-item @if (Request::segment(2) == 'class' || Request::segment(2) == 'subject' || Request::segment(2) == 'timetable' || Request::segment(2) == 'grade') menu-is-opening menu-open @endif ">
            <a href="#" class="nav-link @if (Request::segment(2) == 'class' || Request::segment(2) == 'subject' || Request::segment(2) == 'timetable' || Request::segment(2) == 'grade') active @endif ">
              <i class="nav-icon fas fa-graduation-cap"></i>
              <p>Academics<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none123;">
              <li class="nav-item">
                <a href="{{ url('admin/class/list') }}" class="nav-link @if (Request::segment(2) == 'class' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Class</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('admin/subject/list') }}" class="nav-link @if (Request::segment(2) == 'subject' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Subject</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('admin/grade/list') }}" class="nav-link @if (Request::segment(2) == 'grade' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Grade</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('admin/timetable/list') }}" class="nav-link @if (Request::segment(2) == 'timetable' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Class Timetable</p>
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ url('admin/exam_schedule/list') }}" class="nav-link @if (Request::segment(2) == 'exam_schedule' ) active @endif ">
                <i class="nav-icon fas fa-calendar-o"></i>
                <p>Exam Schedule</p>
            </a>
        </li>
        <li class="nav-item @if (Request::segment(2) == 'communicate' || Request::segment(2) == 'notice_board' ) menu-is-opening menu-open @endif ">
            <a href="#" class="nav-link @if (Request::segment(2) == 'communicate' || Request::segment(2) == 'notice_board' ) active @endif ">
              <i class="nav-icon far fa-envelope"></i>
              <p>Communication<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none123;">
                <li class="nav-item">
                    <a href="{{ url('admin/communicate/list') }}" class="nav-link @if (Request::segment(2) == 'communicate' ) active @endif ">
                        <i class="nav-icon fas fa-arrow-right"></i>
                        <p>Messaging</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/notice_board/list') }}" class="nav-link @if (Request::segment(2) == 'notice_board' ) active @endif ">
                        <i class="nav-icon fas fa-arrow-right"></i>
                        <p>Notice Board</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item @if (Request::segment(2) == 'health_measurement' || Request::segment(2) == 'health_record' ) menu-is-opening menu-open @endif ">
            <a href="#" class="nav-link @if (Request::segment(2) == 'health_measurement' || Request::segment(2) == 'health_record' ) active @endif ">
              <i class="nav-icon fas fa-heartbeat"></i>
              <p>Health<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none123;">
              <li class="nav-item">
                  <a href="{{ url('admin/health_measurement/list') }}" class="nav-link @if (Request::segment(2) == 'health_measurement' ) active @endif ">
                      <i class="nav-icon fas fa-arrow-right"></i>
                      <p>Measurements</p>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="{{ url('admin/health_record/list') }}" class="nav-link @if (Request::segment(2) == 'health_record' ) active @endif ">
                      <i class="nav-icon fas fa-arrow-right"></i>
                      <p>Records</p>
                  </a>
              </li>
            </ul>
        </li>


        {{-- TEACHER --}}
        @elseif (Auth::user()->role == 2)
        <li class="nav-item menu-open">
            <a href="{{ url('teacher/dashboard')}}" class="nav-link @if (Request::segment(2) == 'dashboard' ) active @endif ">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('teacher/student/list') }}" class="nav-link @if (Request::segment(2) == 'student' ) active @endif ">
                <i class="nav-icon fas fa-user"></i>
                <p>Student</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('teacher/assignment/list') }}" class="nav-link @if (Request::segment(2) == 'assignment' ) active @endif ">
                <i class="nav-icon fas fa-pen"></i>
                <p>Assignment</p>
            </a>
        </li>
        <li class="nav-item @if (Request::segment(2) == 'class' || Request::segment(2) == 'subject' || Request::segment(2) == 'class_student' || Request::segment(2) == 'register_marks') menu-is-opening menu-open @endif ">
            <a href="#" class="nav-link @if (Request::segment(2) == 'class' || Request::segment(2) == 'subject' || Request::segment(2) == 'class_student' || Request::segment(2) == 'register_marks') active @endif ">
              <i class="nav-icon fas fa-edit"></i>
              <p>Academics<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none123;">
              <li class="nav-item">
                <a href="{{ url('teacher/class/list') }}" class="nav-link @if (Request::segment(2) == 'class' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Class</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('teacher/subject/list') }}" class="nav-link @if (Request::segment(2) == 'subject' || Request::segment(2) == 'class_student' ) active @endif ">
                  <i class=" fas fa-arrow-right nav-icon"></i>
                  <p>Subject</p>
                </a>
              </li>
            </ul>
        </li>



        {{-- STUDENT  --}}
        @elseif (Auth::user()->role == 3)
        <li class="nav-item menu-open">
            <a href="{{ url('student/dashboard')}}" class="nav-link @if (Request::segment(2) == 'dashboard' ) active @endif ">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('student/class/list') }}" class="nav-link @if (Request::segment(2) == 'class' ) active @endif ">
              <i class="nav-icon fas fa-tree"></i>
              <p>Class</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('student/subject/list') }}" class="nav-link @if (Request::segment(2) == 'subject' ) active @endif ">
              <i class="nav-icon fas fa-tree"></i>
              <p>Subject</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('student/results/list') }}" class="nav-link @if (Request::segment(2) == 'results' ) active @endif ">
              <i class="nav-icon fas fa-tree"></i>
              <p>Results</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('student/grade/list') }}" class="nav-link @if (Request::segment(2) == 'grade' ) active @endif ">
              <i class="nav-icon fas fa-tree"></i>
              <p>Grade</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('student/timetable/list') }}" class="nav-link @if (Request::segment(2) == 'timetable' ) active @endif ">
                <i class="nav-icon fas fa-tree"></i>
                <p>Class Timetable</p>
            </a>
        </li>


        {{-- PARENT --}}
        @elseif (Auth::user()->role == 4)
        <li class="nav-item menu-open">
            <a href="{{ url('parent/dashboard')}}" class="nav-link @if (Request::segment(2) == 'dashboard' ) active @endif ">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
            Users
            <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="{{ url('admin/admin/list') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Users List</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/layout/collapsed-sidebar.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Collapsed Sidebar</p>
            </a>
            </li>
        </ul>
        </li>
        <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
            Charts
            <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="pages/charts/chartjs.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>ChartJS</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/charts/flot.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Flot</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/charts/inline.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Inline</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/charts/uplot.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>uPlot</p>
            </a>
            </li>
        </ul>
        </li>
            <li class="nav-item">
                <a href="pages/UI/ribbons.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ribbons</p>
                </a>
            </li>
        </ul>
        </li>

        @endif

        <li class="nav-item user-panel my-3"></li>
        <li class="nav-item">
            <a href="{{ url('user/profile')}}" class="nav-link @if (Request::segment(2) == 'profile' ) active @endif ">
                <i class="nav-icon fas fa-user-circle"></i>
                <p>Profile</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('logout')}}" class="nav-link">
                <i class="nav-icon fas fa-arrow-left"></i>
                <p>Logout</p>
            </a>
        </li>


        </ul>
      </nav>
    </div>
</aside>






<script>
    $(document).ready(function() {
        user_role();

    });

    function user_role() {

        jQuery.ajax({
            type: "GET",
            url: "/user/role",
            dataType: 'html',
            cache: false,
            success: function(data) {
                $("#user_role").html(data);
            }
        });
    }
</script>
