<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{ $data['header'] }}</h1>
        </div>
        <div class="col-sm-6 float-right">
            <button type="button" onclick="clear_input()"  data-toggle="modal" data-target="#modal-lg" title="Add New" class="btn btn-primary px-3 float-right">Add New</button>
        </div>
      </div>
    </div>
</section>
