<aside class="control-sidebar control-sidebar-dark">
</aside>


<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y')}} <a href="javascript:">School Management System</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
</footer>
