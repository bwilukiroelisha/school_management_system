
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ !empty(strtoupper($data['header']).' | '.strtoupper($data['title'])) ? strtoupper($data['header']).' | '.strtoupper($data['title']) : ''}}</title>
  <link rel="icon" href="{{ asset('assets/dist/img/AdminLTELogo.png')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
</head>

<Style>
  body123{
    background: url('public/assets/dist/img/photo2.png');
    background-attachment: fixed;
    backface-visibility: visible;
    background-size: cover;
    background-repeat: no-repeat;

  }
</Style>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="title text-center h5 text-uppercase mb-5 fw-bold"><b>{{ $data['title'] }}</b></div>
  <div class="card card-outline card-primary">
    <div class="card-header text-center"><h4>{{ strtoupper($data['header']) }}</h4></div>
    <div class="card-body">

        @include('layouts._message')

        <form action="" method="post">

        @csrf

        <div class="input-group mb-3">
            <input type="email" class="form-control" placeholder="Enter your email address" name="email" required>
            <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-envelope"></span>
            </div>
            </div>
        </div>

        <div class="input-group mb-3" style="display:flex;justify-content:space-between";>
            <button type="submit" class="btn btn-primary btn-block">Forgot</button>
        </div>
        </form>
        <p class="mb-1"><a href="{{ url('/') }}">Login</a></p>
    </div>
  </div>
</div>


<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

<script>
$('.alert').fadeOut(7000);
</script>
</body>
</html>
