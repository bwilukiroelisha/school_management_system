<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('teacher_students', function (Blueprint $table) {
            $table->string('remark')->nullable()->after('subject_id');
            $table->string('total')->nullable()->after('subject_id');
            $table->string('exam')->nullable()->after('subject_id');
            $table->string('test_work')->nullable()->after('subject_id');
            $table->string('assignment_work')->nullable()->after('subject_id');
            $table->string('class_work')->nullable()->after('subject_id');

        });


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('teacher_students', function (Blueprint $table) {
            //
        });
    }
};
