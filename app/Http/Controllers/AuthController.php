<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPasswordMail;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Login'
        ];
        $user = Auth::user();

        if(!empty(Auth::check()))
        {
            if ($user->role == 1)
            {
                return redirect('admin/dashboard');
            }
            else if ($user->role == 2)
            {
                return redirect('teacher/dashboard');
            }
            else if ($user->role == 3)
            {
                return redirect('student/dashboard');
            }
            else if ($user->role == 4)
            {
                return redirect('parent/dashboard');
            }

        }else{
            return view('auth.login', compact('data'));

        }
        return view('auth.login', compact('data'));

    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = Auth::user();

            if ($user->role == 1)
            {
                return redirect('admin/dashboard');
            }
            else if ($user->role == 2)
            {
                return redirect('teacher/dashboard');
            }
            else if ($user->role == 3)
            {
                return redirect('student/dashboard');
            }
            else if ($user->role == 4)
            {
                return redirect('parent/dashboard');
            }
            else
            {
                return redirect('/');
            }
        }

        return redirect()->back()->with('error', 'Incorrect Email or Password');
    }

    public function forgot_password()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Forgot Password'
        ];
        return view('auth.reset', compact('data'));
    }

    public function Postforgot_password(Request $request)
    {
        $user = AdminModel::checkEmail($request->email);

        if(!empty($user))
        {
            $user->remember_token = Str::random(30);
            $user->save();
            Mail::to($user->email)->send(new ForgotPasswordMail($user));
            return redirect()->back()->with("success", "Please check your email and reset your password");

        }
        else
        {
            return redirect()->back()->with("error", "Email not found in the system");
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function profile()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'My Profile'
        ];
        return view('profile.index', compact('data'));
    }

    public function userRole()
    {
        $data = AdminModel::getUserRole();
        return $data;
    }

}
