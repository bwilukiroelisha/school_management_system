<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HealthMeasurementModel;
use App\Models\HealthRecordModel;
use App\Models\StudentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class HealthRecordController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Health Record'
        ];

        $students = StudentModel::getStudentList();
        $measurements = HealthMeasurementModel::getList();
        return view('admin.health_record.list', compact('data','students','measurements'));
    }

    public function listView()
    {
        $data = HealthRecordModel::getList();
        return view('admin.health_record.list_view', compact('data'));
    }


    public function addList(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $student_id = $request->input('student_id');
            $measurements = $request->except(['_token', 'hidden_id', 'student_id', 'status']);
            $user_id = Auth::user()->id;

            if(empty($hidden_id)) {
                foreach ($measurements as $measurement_id => $measurement_value) {

                    $saveData = [
                        'student_id' => $student_id,
                        'measurement_id' => $measurement_id,
                        'value' => $measurement_value,
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    // Save data
                    DB::table('health_records')->insert($saveData);
                    $message = 'Health record saved successfully';
                }
            } else {
                foreach ($measurements as $measurement_id => $measurement_value) {
                    $saveData = [
                        'student_id' => $student_id,
                        'measurement_id' => $measurement_id,
                        'value' => $measurement_value,
                        'updated_by' => $user_id,
                        'updated_at' => now(),
                    ];

                    $condition = [
                        'id' => Crypt::decrypt($hidden_id),
                        'archive' => 0,
                    ];

                    // Update data
                    DB::table('health_records')->where($condition)->update($saveData);
                    $message = 'Health measurement updated successfully';
                }
            }

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function deleteList($id)
    {
        try{
            $data = HealthRecordModel::deleteHRecord($id);
            return response()->json(['status' => 200, 'message' =>"Health record deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function editList($id)
    {
        $data = HealthRecordModel::findHRecord($id);
        // dd($data);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

}
