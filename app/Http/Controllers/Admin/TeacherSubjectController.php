<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClassModel;
use App\Models\ClassSubjectModel;
use App\Models\SubjectModel;
use App\Models\TeacherModel;
use App\Models\TeacherSubjectModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class TeacherSubjectController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Teacher Subject'
        ];

        $teacher = TeacherModel::getTeacherList();
        $subject = SubjectModel::getSubjectList();

        return view('admin.teacher_subject.list', compact('data','teacher','subject'));
    }

    public function listView()
    {
        $data = TeacherSubjectModel::getTeacherSubjectList();
        return view('admin.teacher_subject.list_view', compact('data'));
    }


    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $teacher_id = $request->input('teacher_id');
            $subject_id = $request->input('subject_id');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;


            if(empty($hidden_id)):
                foreach ($subject_id as $subject) {
                    $saveData = [
                        'teacher_id' => $teacher_id,
                        'subject_id' => $subject,
                        'status' => $status,
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    ## Save teacher_subject data
                    DB::table('teacher_subjects')->insert($saveData);
                    $message='Assigned subject saved successfully';
                }

            else:

                foreach ($subject_id as $subject) {
                    $saveData = [
                        'teacher_id' => $teacher_id,
                        'subject_id' => $subject,
                        'status' => $status,
                        'updated_by' => $user_id,
                    ];

                    $condition=[
                        'id'=>Crypt::decrypt($hidden_id),
                        'archive'=>0
                    ];

                    ## Save teacher_subject data
                    DB::table('teacher_subjects')->where($condition)->update($saveData);
                    $message='Assigned teacher subject updated successfully';
                }

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = TeacherSubjectModel::deleteTeacherSubject($id);
            return response()->json(['status' => 200, 'message' =>"Assigned teacher to subject deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= TeacherSubjectModel::findTeacherSubject($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

    public function classStudent($class_id)
    {
        $subject = SubjectModel::findSubject($class_id);

        $data = [
            'title' => 'School Management System',
            'header' => 'SUBJECT'.' : '.$subject->name
        ];

        return view('teacher.class_student', compact('data','subject'));
    }

    public function classStudentView($subject_id)
    {
        $teacher_id = Auth::user()->id;
        $data = TeacherSubjectModel::myClassStudent($teacher_id, $subject_id);
        return view('teacher.class_student_view', compact('data'));
    }

}
