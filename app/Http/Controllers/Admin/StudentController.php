<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClassModel;
use Illuminate\Http\Request;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Student'
        ];

        $class = ClassModel::getClassList();

        return view('admin.student.list', compact('data','class'));
    }

    public function listView()
    {
        $data = StudentModel::getStudentList();
        return view('admin.student.list_view', compact('data'));
    }


    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $fname = $request->input('fname');
            $mname = $request->input('mname');
            $lname = $request->input('lname');
            $gender = $request->input('gender');
            $phone = $request->input('phone');
            $email = $request->input('email');
            $admission_no = $request->input('admission_no');
            $admission_date = $request->input('admission_date');
            $class_id = $request->input('class_id');
            $religion = $request->input('religion');
            $dob = $request->input('dob');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;
            $role = 3;

            ## Handling image upload
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move('upload/profile/', $imageName);
            } else {
                $imageName = null;
            }


            if(empty($hidden_id)):
                $saveData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'image' => $imageName,
                    'dob' => $dob,
                    'admission_no' => $admission_no,
                    'admission_date' => $admission_date,
                    'class_id' => $class_id,
                    'religion' => $religion,
                    'status' => $status,
                    'role' => $role,
                    'password' => Hash::make($phone),
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                // Save data
                DB::table('users')->insert($saveData);
                $message='Student saved successfully';

            else:

                $saveData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'image' => $imageName,
                    'dob' => $dob,
                    'admission_no' => $admission_no,
                    'admission_date' => $admission_date,
                    'class_id' => $class_id,
                    'religion' => $religion,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('users')->where($condition)->update($saveData);
                $message='Student updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = StudentModel::deleteStudent($id);
            return response()->json(['status' => 200, 'message' =>"Student deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= StudentModel::findStudent($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }


    ## ASSIGN STUDENT TO SUBJECTS
    public function studentSubject()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Student Subjects'
        ];

        $students = StudentModel::getStudentList();
        $subjects = SubjectModel::getSubjectList();

        return view('admin.student_subject.list', compact('data','students','subjects'));
    }

    public function studentSubjectView()
    {
        $data = StudentModel::assignStudentSubject();
        // foreach ($data as $studentId => $subjects) {
        //     $student = $subjects->first(); // all records in $subjects have the same student info
        //     echo "<div style='border: 1px solid #000; padding: 10px; margin-bottom: 10px;'>";
        //     echo "<strong>Student ID:</strong> " . $studentId . "<br>";
        //     echo "<strong>First Name:</strong> " . $student->fname . "<br>";
        //     echo "<strong>Middle Name:</strong> " . $student->mname . "<br>";
        //     echo "<strong>Last Name:</strong> " . $student->lname . "<br>";
        //     echo "<strong>Admission No:</strong> " . $student->admission_no . "<br>";
        //     echo "<strong>Enrolled Subjects:</strong><br>";
        //     echo "<ul>";
        //     foreach ($subjects as $subject) {
        //         echo "<li>Subject Name: " . $subject->subject_name . ", Type: " . $subject->subject_type . "</li>";
        //     }
        //     echo "</ul>";
        //     echo "</div>";
        // }
        // die;
        return view('admin.student_subject.list_view', compact('data'));
    }

    public function addStudentSubject(Request $request)
    {

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $student_id = $request->input('student_id');
            $subject_id = $request->input('subject_id');
            $status = $request->input('status');
            $user_id = Auth::user()->id;


            if(empty($hidden_id)):
                foreach ($subject_id as $subject) {
                    $saveData = [
                        'student_id' => $student_id,
                        'subject_id' => $subject,
                        'status' => $status,
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    ## Save data
                    DB::table('student_subjects')->insert($saveData);
                    $message='Assigned student subject saved successfully';
                }

            else:

                foreach ($subject_id as $subject) {
                    $saveData = [
                        'student_id' => $student_id,
                        'subject_id' => $subject,
                        'status' => $status,
                        'updated_by' => $user_id,
                    ];

                    $condition=[
                        'id'=>Crypt::decrypt($hidden_id),
                        'archive'=>0
                    ];

                    ## Save data
                    DB::table('student_subjects')->where($condition)->update($saveData);
                    $message='Assigned student subject updated successfully';
                }

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    // public function delete($id)
    // {
    //     try{
    //         $data = ClassSubjectModel::deleteClassSubject($id);
    //         return response()->json(['status' => 200, 'message' =>"Assigned class deleted successfully"]);
    //     } catch (\Exception $e) {
    //         return response()->json(['status' => 500, 'message' => $e->getMessage()]);
    //     }
    // }

    // public function edit($id)
    // {
    //     $data= ClassSubjectModel::findClassSubject($id);
    //     echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    // }


}
