<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminModel;
use App\Models\CommunicateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class CommunicateController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Communicate'
        ];

        $users = AdminModel::searchUser();
        $msg = CommunicateModel::getList();
        return view('admin.communicate.list', compact('data','users','msg'));
    }

    public function listView()
    {
        $data = CommunicateModel::getList();
        return view('admin.communicate.list_view', compact('data'));
    }


    public function add(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $subject = $request->input('subject');
            $user = $request->input('user');
            $message_to = $request->input('message_to');
            $message = $request->input('message');
            $user_id = Auth::user()->id;

            $communicate = new CommunicateModel();
            $comm_id = $communicate->id;


            ## Handling image upload
            if ($request->hasFile('attachment')) {
                $image = $request->file('attachment');
                $originalName = $image->getClientOriginalName();
                $imageName = time() . '_' . $originalName;
                $image->move('attachment/communicate/', $imageName);
            } else {
                $imageName = null;
            }

            if(empty($hidden_id)):

                $saveData1 = [
                    'subject' => $subject,
                    'users' => $user,
                    'message' => $message,
                    'attachment' => $imageName,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                // Save data
                DB::table('communicates')->insert($saveData1);
                $message='Message send successfully';


                foreach ($message_to as $msg_to) {
                    $saveData2 = [
                        'comm_id' => $comm_id,
                        'msg_to_id' => $msg_to,
                    ];
                    // Save data
                    DB::table('comm_msg_to')->insert($saveData2);
                }

            else:

                // foreach ($message_to as $msg_to) {

                //     $saveData = [
                //         'subject' => $subject,
                //         'users' => $user,
                //         'message_to' => $msg_to,
                //         'message' => $message,
                //         'created_by' => $user_id,
                //         'updated_by' => $user_id,
                //         'created_at' => now(),
                //         'updated_at' => now(),
                //     ];

                //     $condition=[
                //         'id'=>Crypt::decrypt($hidden_id),
                //         'archive'=>0
                //     ];

                //     ## Save data
                //     DB::table('communicates')->where($condition)->update($saveData);
                //     $message='Message updated successfully';

                // }

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = CommunicateModel::deleteEmail($id);
            return response()->json(['status' => 200, 'message' =>"Email deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data = CommunicateModel::findEmail($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

    public function readMail($id)
    {
        $data = CommunicateModel::readMail($id);
        return response()->json(['data'=>$data]);
    }



}
