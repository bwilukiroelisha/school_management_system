<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentModel;
use App\Models\ClassModel;
use App\Models\SubjectModel;
use App\Models\TeacherModel;
use App\Models\ParentModel;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Dashboard'
        ];



        $student = StudentModel::getStudentList();
        $class = ClassModel::getClassList();
        $subject = SubjectModel::getSubjectList();
        $teacher = TeacherModel::getTeacherList();
        $parent = ParentModel::getParentList();

        if(Auth::user()->role == 1)
        {

            return view('admin.dashboard', compact('data','class','student','subject','teacher','parent'));

        }
        else if(Auth::user()->role == 2)
        {
            return view('teacher.dashboard', compact('data','class','student','subject','teacher','parent'));

        }
        else if(Auth::user()->role == 3)
        {
            return view('student.dashboard', compact('data','class','student','subject','teacher','parent'));

        }
        else if(Auth::user()->role == 4)
        {
            return view('parent.dashboard', compact('data','class','student','subject','teacher','parent'));

        }
        else{
            return redirect(url('/'));
        }

    }
}
