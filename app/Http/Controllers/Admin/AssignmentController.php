<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AssignmentModel;
use App\Models\TeacherModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class AssignmentController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Assignment'
        ];

        $teacher_id = Auth::user()->id;
        $teacher_subjects = TeacherModel::mySubject($teacher_id);
        return view('teacher.assignment', compact('data','teacher_subjects'));
    }

    public function listView()
    {
        $data = AssignmentModel::getAssignment();
        return view('teacher.assignment_view', compact('data'));
    }


    public function add(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $subject_id = $request->input('subject_id');
            $template = $request->input('template');
            $status = $request->input('status');
            $user_id = Auth::user()->id;

            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $filePath = time() . '.' . $image->getClientOriginalExtension();
                $image->move('attachment/assignments/', $filePath);
            } else {
                $filePath = null;
            }


            if(empty($hidden_id)):
                $saveData = [
                    'subject_id' => $subject_id,
                    'file' => $filePath,
                    'template' => $template,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                DB::table('assignments')->insert($saveData);
                $message='Assignment saved successfully';

            else:

                $saveData = [
                    'subject_id' => $subject_id,
                    'template' => $template,
                    'file' => $filePath,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('assignments')->where($condition)->update($saveData);
                $message='Assignment updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }


    public function delete($id)
    {
        try{
            $data = AssignmentModel::deleteAssignment($id);
            return response()->json(['status' => 200, 'message' =>"Assignment deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= AssignmentModel::findAssignment($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

}
