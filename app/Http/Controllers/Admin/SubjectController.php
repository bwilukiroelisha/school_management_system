<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use App\Models\TeacherModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
class SubjectController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Subject'
        ];

        return view('admin.subject.list', compact('data'));
    }

    public function listView()
    {
        $data = SubjectModel::getSubjectList();
        return view('admin.subject.list_view', compact('data'));
    }

    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $name = $request->input('name');
            $type = $request->input('type');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;

            if(empty($hidden_id)):
                $saveData = [
                    'name' => $name,
                    'type' => $type,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save subject data
                DB::table('subjects')->insert($saveData);
                $message='Subject saved successfully';

            else:

                $saveData = [
                    'name' => $name,
                    'type' => $type,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save admin data
                DB::table('subjects')->where($condition)->update($saveData);
                $message='Subject updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = SubjectModel::deleteSubject($id);
            return response()->json(['status' => 200, 'message' =>"Subject deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= SubjectModel::findSubject($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }


    ## TEACHER'S SUBJECTS
    public function mySubject()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Subjects'
        ];

        return view('teacher.subject', compact('data'));
    }

    public function mySubjectView()
    {
        $teacher_id = Auth::user()->id;
        $data = TeacherModel::mySubject($teacher_id);
        return view('teacher.subject_view', compact('data'));
    }


    ## STUDENT'S SUBJECTS
    public function studentSubject()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Subject'
        ];

        return view('student.subject', compact('data'));
    }

    public function studentSubjectView()
    {
        $student_id = Auth::user()->id;
        $data = StudentModel::getStudentSubject($student_id);
        return view('student.subject_view', compact('data'));
    }


    ## STUDENT'S RESULTS
    public function studentResults()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Result'
        ];

        $student_id = Auth::user()->id;
        return view('student.results', compact('data','student_id'));
    }

    public function studentResultsView()
    {
        $student_id = Auth::user()->id;
        $data = StudentModel::getStudentResults($student_id);
        if(!empty($data))
        {
            return view('student.results_view', compact('data'));

        }else{
            abort(404);
        }
    }
    public function printResults($student_id)
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Results'
        ];

        $results = StudentModel::printStudentResults($student_id);
        if(!empty($results))
        {
            return view('student.print', compact('data','results'));

        }else{
            abort(404);
        }
    }



}
