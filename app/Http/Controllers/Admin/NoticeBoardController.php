<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NoticeBoardModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class NoticeBoardController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management',
            'header' => 'Notice Board'
        ];

        return view('admin.notice_board.list', compact('data'));
    }

    public function listView()
    {
        $data = NoticeBoardModel::getList();
        return view('admin.notice_board.list_view', compact('data'));
    }

    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $title = $request->input('title');
            $notice_date = $request->input('notice_date');
            $published_date = $request->input('published_date');
            $message_to = $request->input('message_to');
            $message = $request->input('message');
            $status = $request->input('status');
            $user_id = Auth::user()->id;

            ## Handling image upload
            if ($request->hasFile('attachment')) {
                $attachment = $request->file('attachment');
                $attachmentName = time() . '.' . $attachment->getClientOriginalExtension();
                $attachment->move('upload/notices/', $attachmentName);
            } else {
                $attachmentName = null;
            }

            if(empty($hidden_id)):
                $saveData = [
                    'title' => $title,
                    'notice_date' => $notice_date,
                    'published_date' => $published_date,
                    'message' => $message,
                    'attachment' => $attachmentName,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                $noticeId = DB::table('notice_boards')->insertGetId($saveData);
                foreach ($message_to as $recipient) {
                    DB::table('notice_recipients')->insert([
                        'notice_id' => $noticeId,
                        'recipient_type' => $recipient,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                }

                $message='Notice saved successfully';

            else:

                $saveData = [
                    'title' => $title,
                    'notice_date' => $notice_date,
                    'published_date' => $published_date,
                    'message' => $message,
                    'attachment' => $attachmentName,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('notice_boards')->where($condition)->update($saveData);
                $message='Notice updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = NoticeBoardModel::deleteNotice($id);
            return response()->json(['status' => 200, 'message' =>"Notice deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data = NoticeBoardModel::findNotice($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }


}
