<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ParentModel;
use App\Models\StudentModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ParentController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Parent'
        ];

        return view('admin.parent.list', compact('data'));
    }

    public function listView()
    {
        $data = ParentModel::getParentList();
        return view('admin.parent.list_view', compact('data'));
    }


    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $fname = $request->input('fname');
            $mname = $request->input('mname');
            $lname = $request->input('lname');
            $gender = $request->input('gender');
            $phone = $request->input('phone');
            $email = $request->input('email');
            $current_address = $request->input('current_address');
            $permanent_address = $request->input('permanent_address');
            $work = $request->input('work');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;
            $role = 4;

            ## Handling image upload
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move('upload/profile/', $imageName);
            } else {
                $imageName = null;
            }

            if(empty($hidden_id)):
                $parentData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'work' => $work,
                    'current_address' => $current_address,
                    'permanent_address' => $permanent_address,
                    'status' => $status,
                    'image' => $imageName,
                    'role' => $role,
                    'password' => Hash::make($phone),
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save Parent data
                DB::table('users')->insert($parentData);
                $message='Parent saved successfully';

            else:

                $parentData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'image' => $imageName,
                    'current_address' => $current_address,
                    'permanent_address' => $permanent_address,
                    'work' => $work,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save Parent data
                DB::table('users')->where($condition)->update($parentData);
                $message='Parent updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = ParentModel::deleteParent($id);
            return response()->json(['status' => 200, 'message' =>"Parent deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= ParentModel::findParent($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

    public function myStudent($id)
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Parent Students'
        ];

        $parent_id = Crypt::decrypt($id);

        $parent = ParentModel::findParent($parent_id);
        return view('admin.parent.parent_student', compact('data','parent'));
    }

    public function myStudentView($parent_id)
    {
        $student = ParentModel::getParentMyStudent($parent_id);
        return view('admin.parent.parent_student_view', compact('student'));
    }

    public function searchStudent($searchText)
    {
        $data = StudentModel::searchStudent($searchText);

        $view = '';

		foreach ($data as $sval) {
            $view .= '<a><li style="background-color: #842530;color:white;  border: 1px solid #842530; padding: 5px; list-style: none; cursor: pointer; z-index:1;width:100%;" onclick="addText(\'' . $sval->id . '\', \'' . $sval->fname . '\', \'' . $sval->mname . '\',\'' . $sval->lname . '\',\'' .$sval->admission_no. '\')">'
            . $sval->fname." ".$sval->mname." ".$sval->lname ." - ".$sval->admission_no. '</li></a>';
        }

        echo $view;
    }

    public function addMyStudent(Request $request)
    {

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $parent_id = $request->input('parent_id');
            $student_id = $request->input('student_id');

            if(empty($hidden_id)):

                $saveData = [
                    'parent_id' => $parent_id,
                ];

                $condition=[
                    'id'=>$student_id,
                    'archive'=>0
                ];

                DB::table('users')->where($condition)->update($saveData);
                $message='Assign Student to Parent saved successfully';

            else:

                $saveData = [
                    'parent_id' => $parent_id,
                ];

                $condition=[
                    'id'=>$student_id,
                    'archive'=>0
                ];

                ## Save data
                DB::table('users')->where($condition)->update($saveData);
                $message='Assign Student Parent updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }

    }

    public function deleteParentStudent($id)
    {
        try{
            $data = StudentModel::deleteParentStudent($id);
            return response()->json(['status' => 200, 'message' =>"Parent student deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }


}
