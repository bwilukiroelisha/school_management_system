<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubjectModel;
use App\Models\TeacherModel;
use App\Models\TeacherSubjectModel;
use App\Models\StudentModel;
use App\Models\GradeModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TeacherController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Teacher'
        ];

        ## $class = TeacherModel::getClassList();

        return view('admin.teacher.list', compact('data'));
    }

    public function listView()
    {
        $data = TeacherModel::getTeacherList();
        return view('admin.teacher.list_view', compact('data'));
    }


    public function add(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $fname = $request->input('fname');
            $mname = $request->input('mname');
            $lname = $request->input('lname');
            $gender = $request->input('gender');
            $phone = $request->input('phone');
            $email = $request->input('email');
            $current_address = $request->input('current_address');
            $permanent_address = $request->input('permanent_address');
            $religion = $request->input('religion');
            $marital_status = $request->input('marital_status');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;
            $role = 2;

            ## Handling image upload
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move('upload/profile/', $imageName);
            } else {
                $imageName = null;
            }

            if(empty($hidden_id)):
                $saveData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'image' => $imageName,
                    'marital_status' => $marital_status,
                    'current_address' => $current_address,
                    'permanent_address' => $permanent_address,
                    'religion' => $religion,
                    'status' => $status,
                    'role' => $role,
                    'password' => Hash::make($phone),
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save teacher data
                DB::table('users')->insert($saveData);
                $message='Teacher saved successfully';

            else:

                $saveData = [
                    'fname' => $fname,
                    'mname' => $mname,
                    'lname' => $lname,
                    'gender' => $gender,
                    'phone' => $phone,
                    'email' => $email,
                    'image' => $imageName,
                    'marital_status' => $marital_status,
                    'current_address' => $current_address,
                    'permanent_address' => $permanent_address,
                    'religion' => $religion,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save teacher data
                DB::table('users')->where($condition)->update($saveData);
                $message='Teacher updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = TeacherModel::deleteTeacher($id);
            return response()->json(['status' => 200, 'message' =>"Teacher deleted successfull"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= TeacherModel::findTeacher($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

    public function mySubject($id)
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Teacher Subjects'
        ];

        $teacher_id = Crypt::decrypt($id);

        $teacher = TeacherModel::findTeacher($teacher_id);
        $subject = SubjectModel::getSubjectList();
        return view('admin.teacher.teacher_subject', compact('data','teacher','subject'));
    }

    public function mySubjectView($teacher_id)
    {
        $subjects = TeacherModel::getTeacherSubject($teacher_id);
        return view('admin.teacher.teacher_subject_view', compact('subjects'));
    }
    public function deleteTeacherSubject($id)
    {
        try{
            $data = TeacherModel::deleteTeacherSubject($id);
            return response()->json(['status' => 200, 'message' =>"Teacher subject deleted successfull"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }

    }

    public function addMySubject(Request $request)
    {

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $teacher_id = $request->input('teacher_id');
            $subject_id = $request->input('subject_id');
            $user_id = Auth::user()->id;


            if(empty($hidden_id)):

                foreach ($subject_id as $subject) {

                    $saveData = [
                        'teacher_id' => $teacher_id,
                        'subject_id' => $subject,
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    DB::table('teacher_subjects')->insert($saveData);
                    $message='Subject assigned to teacher saved successfully';
                }

            else:


                foreach ($subject_id as $subject) {

                    $saveData = [
                        'teacher_id' => $teacher_id,
                        'subject_id' => $subject,
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    $condition=[
                        'teacher_id' => $teacher_id,
                        'subject_id' => $subject,
                        'archive'=>0
                    ];

                    DB::table('teacher_subjects')->where($condition)->update($saveData);
                    $message='Subject assigned to teacher updated successfully';
                }


            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }


    ## TEACHER'S STUDENTS
    public function myStudent()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Student'
        ];

        return view('teacher.student', compact('data'));
    }

    public function myStudentView()
    {
        $teacher_id = Auth::user()->id;
        $data = TeacherModel::myStudent($teacher_id);
        return view('teacher.student_view', compact('data'));
    }

    public function myStudentUpdate($id)
    {
        try{
            $data = TeacherModel::updateStudent($id);
            return response()->json(['status' => 200, 'message' =>"Student status updated successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function addMarks(Request $request, $subject_id,$student_id)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $class = $request->input('class');
            $assignment = $request->input('assignment');
            $test = $request->input('test');
            $exam = $request->input('exam');
            $status = $request->input('status');
            $user_id = Auth::user()->id;

            $total = ($class + $assignment + $test + $exam);
            $grade = GradeModel::where('from', '<=', $total)->where('to', '>=', $total)->first();

            if(empty($hidden_id)):
                $saveData = [
                    'class_work' => $class,
                    'assignment_work' => $assignment,
                    'test_work' => $test,
                    'exam' => $exam,
                    'total' => $total,
                    'grade' => $grade->name,
                    'remark' => $grade->remark,
                    'status' => $status,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];


                $condition=[
                    'student_id'=>$student_id,
                    'subject_id'=>$subject_id,
                    'teacher_id'=>$user_id,
                    'archive'=>0
                ];

                ## Save data
                DB::table('teacher_students')->where($condition)->update($saveData);
                $message='Marks saved successfully';

            else:

                $saveData = [
                    'class_work' => $class,
                    'assignment_work' => $assignment,
                    'test_work' => $test,
                    'exam' => $exam,
                    'total' => $total,
                    'grade' => $grade->name,
                    'remark' => $grade->remark,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'student_id'=>$student_id,
                    'subject_id'=>$subject_id,
                    'teacher_id'=>$user_id,
                    'archive'=>0
                ];

                ## Save data
                DB::table('teacher_students')->where($condition)->update($saveData);
                $message='Marks updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }

    }

    public function editMarks($id, $subject_id,$student_id)
    {
        $data = TeacherSubjectModel::updateMarks($id,$subject_id,$student_id);
        return response()->json(['status' => 200, 'data' => $data]);
    }

    public function calculateAverage(Request $request, $studentId)
    {
        $grades = GradeModel::where('student_id', $studentId)->pluck('grade')->toArray();

        $average = count($grades) > 0 ? array_sum($grades) / count($grades) : 0;

        return response()->json(['average' => $average]);
    }

    public function calculateFinalScore(Request $request, $studentId)
    {
        $grades = GradeModel::where('student_id', $studentId)->pluck('grade')->toArray();

        $finalScore = count($grades) > 0 ? array_sum($grades) / count($grades) : 0;

        return response()->json(['final_score' => $finalScore]);
    }

    public function generateProgressReport(Request $request, $studentId)
    {
        $student = StudentModel::find($studentId);
        $grades = GradeModel::where('student_id', $studentId)->get();

        $progressReport = [
            'student_name' => $student->name,
            'grades' => $grades->map(function ($grade) {
                return [
                    'assignment' => $grade->assignment->name,
                    'grade' => $grade->grade,
                    'date' => $grade->created_at->toDateString()
                ];
            })
        ];

        return response()->json($progressReport);
    }
}
