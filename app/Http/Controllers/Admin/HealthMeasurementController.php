<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HealthMeasurementModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class HealthMeasurementController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Health Measurement'
        ];

        return view('admin.health_measurement.list', compact('data'));
    }

    public function listView()
    {
        $data = HealthMeasurementModel::getList();
        return view('admin.health_measurement.list_view', compact('data'));
    }


    public function addList(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $name = $request->input('name');
            $remark = $request->input('remark');
            $user_id = Auth::user()->id;

            if(empty($hidden_id)):

                $saveData = [
                    'name' => $name,
                    'remark' => $remark,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                DB::table('health_measurements')->insert($saveData);
                $message='Health measurement saved successfully';

            else:

                    $saveData = [
                        'name' => $name,
                        'remark' => $remark,
                        'updated_by' => $user_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];

                    $condition=[
                        'id'=>Crypt::decrypt($hidden_id),
                        'archive'=>0
                    ];

                    ## Save data
                    DB::table('health_measurements')->where($condition)->update($saveData);
                    $message='Health measurement updated successfully';


            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function deleteList($id)
    {
        try{
            $data = HealthMeasurementModel::deleteHealthMeasurement($id);
            return response()->json(['status' => 200, 'message' =>"Health measurement deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function editList($id)
    {
        $data = HealthMeasurementModel::findHealthMeasurement($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }


}
