<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GradeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class GradeController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management',
            'header' => 'Grade'
        ];

        return view('admin.grade.list', compact('data'));
    }

    public function listView()
    {
        $data = GradeModel::getList();
        return view('admin.grade.list_view', compact('data'));
    }

    public function add(Request $request){

        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $name = $request->input('name');
            $from = $request->input('from');
            $to = $request->input('to');
            $remark = $request->input('remark');
            $status = $request->input('status');
            $user_id = Auth::user()->id;

            if(empty($hidden_id)):
                $saveData = [
                    'name' => $name,
                    'from' => $from,
                    'to' => $to,
                    'remark' => $remark,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                DB::table('grades')->insert($saveData);
                $message='Grade saved successfully';

            else:

                $saveData = [
                    'name' => $name,
                    'from' => $from,
                    'to' => $to,
                    'remark' => $remark,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('grades')->where($condition)->update($saveData);
                $message='Grade updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = GradeModel::deleteItem($id);
            return response()->json(['status' => 200, 'message' =>"Grade deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data = GradeModel::findItem($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }



    ## STUDENT VIEW GRADES
    public function studentGrade()
    {
        $data = [
            'title' => 'School Management',
            'header' => 'Grade'
        ];

        return view('student.grade', compact('data'));
    }

    public function studentGradeView()
    {
        $data = GradeModel::getList();
        return view('student.grade_view', compact('data'));
    }


}
