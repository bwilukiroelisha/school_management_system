<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GradeModel;
use App\Models\ClassModel;
use App\Models\SubjectModel;
use App\Models\TimetableModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class ClassTimeTableController extends Controller
{
    public function list()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Timetable'
        ];

        $days = SubjectModel::getWeekDays();
        $subject = SubjectModel::getSubjectList();
        $class = ClassModel::getClassList();

        return view('admin.timetable.list', compact('data','days','subject','class'));
    }

    public function listView()
    {
        $data = TimetableModel::getTimetable();
        return view('admin.timetable.list_view', compact('data'));
    }

    public function add(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $day_id = $request->input('day_id');
            $subject_id = $request->input('subject_id');
            $class_id = $request->input('class_id');
            $finish_time = $request->input('finish_time');
            $start_time = $request->input('start_time');
            $status = $request->input('status1');
            $user_id = Auth::user()->id;

            if(empty($hidden_id)):
                $saveData = [
                    'day_id' => $day_id,
                    'subject_id' => $subject_id,
                    'class_id' => $class_id,
                    'start_time' => $start_time,
                    'finish_time' => $finish_time,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                DB::table('timetable')->insert($saveData);
                $message='Timetable saved successfully';

            else:

                $saveData = [
                    'day_id' => $day_id,
                    'subject_id' => $subject_id,
                    'class_id' => $class_id,
                    'start_time' => $start_time,
                    'finish_time' => $finish_time,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('timetable')->where($condition)->update($saveData);
                $message='Timetable updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function delete($id)
    {
        try{
            $data = TimetableModel::deleteItem($id);
            return response()->json(['status' => 200, 'message' =>"Timetable deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $data= TimetableModel::findItem($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }

    public function viewTimetable($subjec_id)
    {
        $data = TimetableModel::viewTimetable($subjec_id);
        $html = '';
        if(!empty($data))
        {
            foreach ($data as $val) {
                $html .= '<tr>
                            <td>'.$val->day.'</td>
                            <td>'.$val->start_time.'</td>
                            <td>'.$val->finish_time.'</td>
                            <td>'.$val->class.'</td>
                        </tr>';
            }
        }
        else
        {
            $html .= '<tr>
                        <td colspan="100%" class="text-center">Not Arranged.</td>
                    </tr>';
        }

        $json['html'] = $html;
        echo json_encode($json);

    }





    ### EXAMINATION SCHEDULE
    public function exam()
    {
        $data = [
            'title' => 'School Management System',
            'header' => 'Examination Schedule'
        ];

        $pass_marks = GradeModel::getList();
        $subject = SubjectModel::getSubjectList();
        $class = ClassModel::getClassList();

        return view('admin.exam_schedule.list', compact('data','pass_marks','subject','class'));
    }

    public function examView()
    {
        $data = TimetableModel::showExam();
        return view('admin.exam_schedule.list_view', compact('data'));
    }

    public function saveExam(Request $request)
    {
        try {
            DB::beginTransaction();

            $hidden_id = $request->input('hidden_id');
            $subject_id = $request->input('subject_id');
            $day = $request->input('day');
            $start_time = $request->input('start_time');
            $finish_time = $request->input('finish_time');
            $class_id = $request->input('class_id');
            $full_marks = $request->input('full_marks');
            $pass_marks = $request->input('pass_marks');
            $status = $request->input('status');
            $user_id = Auth::user()->id;

            if(empty($hidden_id)):
                $saveData = [
                    'date' => $day,
                    'subject_id' => $subject_id,
                    'class_id' => $class_id,
                    'start_time' => $start_time,
                    'finish_time' => $finish_time,
                    'full_marks' => $full_marks,
                    'pass_marks' => $pass_marks,
                    'status' => $status,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                ## Save data
                DB::table('exam_schedules')->insert($saveData);
                $message='Exam schedule saved successfully';

            else:

                $saveData = [
                    'date' => $day,
                    'subject_id' => $subject_id,
                    'class_id' => $class_id,
                    'start_time' => $start_time,
                    'finish_time' => $finish_time,
                    'full_marks' => $full_marks,
                    'pass_marks' => $pass_marks,
                    'status' => $status,
                    'updated_by' => $user_id,
                ];

                $condition=[
                    'id'=>Crypt::decrypt($hidden_id),
                    'archive'=>0
                ];

                ## Save data
                DB::table('exam_schedules')->where($condition)->update($saveData);
                $message='Exam schedule updated successfully';

            endif;

            DB::commit();

            return response()->json(['status' => 200, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function deleteExam($id)
    {
        try{
            $data = TimetableModel::deleteExam($id);
            return response()->json(['status' => 200, 'message' =>"Exam schedule deleted successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function editExam($id)
    {
        $data= TimetableModel::findExam($id);
        echo json_encode(['data'=>$data,'id'=>Crypt::encrypt($id)]);
    }


}
