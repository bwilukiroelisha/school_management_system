<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommunicateModel extends Model
{
    use HasFactory;

    protected $table = 'communicates';

    static public function getList()
    {
        return DB::table('communicates')->where('archive',0)->get();
    }

    public static function deleteEmail($id)
    {
        DB::table('communicates')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findEmail($id)
    {
        return DB::table('communicates')->where(['archive' => 0, 'id' => $id])->first();
    }

    static public function readMail($id)
    {
        DB::table('communicates')->where('id', $id)->update(['status' => 1]);
        return DB::table('communicates')->where('id', $id)->first();
    }
}
