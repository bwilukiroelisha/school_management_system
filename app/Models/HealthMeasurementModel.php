<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HealthMeasurementModel extends Model
{
    use HasFactory;

    protected $table = 'health_measurements';

    static public function getList()
    {
        return DB::table('health_measurements')->where(['archive'=>0])->orderBy('name', 'asc')->get();
    }
    public static function deleteHealthMeasurement($id)
    {
        DB::table('health_measurements')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findHealthMeasurement($id)
    {
        return DB::table('health_measurements')->where(['archive' => 0, 'id' => $id])->first();
    }

}
