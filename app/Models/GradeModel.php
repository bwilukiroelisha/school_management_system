<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GradeModel extends Model
{
    use HasFactory;

    protected $table = 'grades';

    static public function getList()
    {
        return DB::table('grades')->where(['archive'=>0])->orderBy('name','asc')->get();
    }
    public static function deleteItem($id)
    {
        DB::table('grades')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findItem($id)
    {
        return DB::table('grades')->where(['archive' => 0, 'id' => $id])->first();
    }
}
