<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StudentModel extends Model
{
    use HasFactory;
    protected $table = 'users';
    static public function getStudentList()
    {
        return DB::table('users')->where(['role'=> 3, 'archive'=>0])->orderBy('id', 'desc')->get();
    }
    public static function deleteStudent($id)
    {
        DB::table('users')->where(['role'=>3,'archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findStudent($id)
    {
        return DB::table('users')->where(['role'=>3, 'archive' => 0, 'id' => $id])->first();
    }

    static public function getStudentParent($parent_id)
    {
        return DB::table('users')->where(['role'=>3,'archive' => 0, 'status' => 0, 'parent_id' => $parent_id])->orderBy('id', 'desc')->get();
    }
    static public function searchStudent($searchText)
    {
        return DB::table('users')
            ->where('role', 3)
            ->where('archive', 0)
            ->where('status', 0)
            ->where(function ($query) use ($searchText) {
                $query->orWhere('fname', 'like', '%' . $searchText . '%')
                    ->orWhere('mname', 'like', '%' . $searchText . '%')
                    ->orWhere('lname', 'like', '%' . $searchText . '%')
                    ->orWhere('admission_no', 'like', '%' . $searchText . '%');
            })->get();
    }
    static public function deleteParentStudent($id)
    {
        DB::table('users')->where(['role' => 3,'archive' => 0, 'id' => $id])->update(['parent_id' => NULL]);
    }

    static public function getStudentSubject($student_id)
    {
        return DB::table('subjects')
                ->join('teacher_students', 'subjects.id', '=', 'teacher_students.subject_id')
                ->where('teacher_students.student_id', $student_id)
                ->where('subjects.archive','=','0')
                ->where('subjects.status','=','0')
                ->select('subjects.*','teacher_students.teacher_id as teacher_id')
                ->get();

    }

    static public function getStudentResults($student_id)
    {
        return DB::table('teacher_students')
                ->join('subjects', 'subjects.id', '=', 'teacher_students.subject_id')
                ->where('teacher_students.student_id', $student_id)
                ->where('teacher_students.archive','=','0')
                ->where('teacher_students.status','=','0')
                ->select('teacher_students.*')
                ->get();

    }

    static public function printStudentResults($student_id)
    {
        return DB::table('teacher_students')
                ->join('subjects', 'subjects.id', '=', 'teacher_students.subject_id')
                ->where('teacher_students.student_id', $student_id)
                ->where('teacher_students.archive','=','0')
                ->where('teacher_students.status','=','0')
                ->select('teacher_students.*')
                ->get();

    }

    static public function assignStudentSubject()
    {
        return DB::table('student_subjects')
                ->join('users', 'student_subjects.student_id', '=', 'users.id')
                ->join('subjects', 'student_subjects.subject_id', '=', 'subjects.id')
                ->where('student_subjects.archive','=','0')
                ->select(
                    'student_subjects.id',
                    'student_subjects.status',
                    'student_subjects.created_at',
                    'student_subjects.created_by',
                    'student_subjects.updated_by',
                    'users.fname',
                    'users.mname',
                    'users.lname',
                    'users.phone',
                    'users.email',
                    'users.gender',
                    'users.admission_no',
                    'subjects.name as subject_name',
                    'subjects.type as subject_type'
                )
                ->get()
                ->groupBy('student_id');

        // return DB::table('student_subjects')->where('archive','=','0')->get();


    }
    public function subjects()
    {
        return $this->belongsToMany(SubjectModel::class, 'student_subjects');
    }
}

