<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TeacherSubjectModel extends Model
{
    use HasFactory;
    protected $table = 'teacher_subjects';
    static public function getTeacherSubjectList()
    {
        return DB::table('teacher_subjects')->where('archive', 0)->orderBy('id', 'desc')->get();
    }
    public static function deleteTeacherSubject($id)
    {
        DB::table('teacher_subjects')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findTeacherSubject($id)
    {
        return DB::table('teacher_subjects')->where(['archive' => 0, 'id' => $id])->first();
    }


    static public function myClassStudent($teacher_id, $subject_id)
    {
        return DB::table('teacher_students')
                ->where('teacher_students.teacher_id', $teacher_id)
                ->where('teacher_students.subject_id', $subject_id)
                ->where('teacher_students.archive','=','0')
                // ->where('teacher_students.status','=','0')
                ->select('teacher_students.*')
                ->get();

    }

    static public function updateMarks($id,$subject_id,$student_id)
    {
        return DB::table('teacher_students')->where(['id'=>$id,'subject_id'=>$subject_id,'student_id'=>$student_id])->first();
    }
}
