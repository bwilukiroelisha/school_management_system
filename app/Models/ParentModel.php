<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ParentModel extends Model
{
    use HasFactory;
    protected $table = 'users';
    static public function getParentList()
    {
        return DB::table('users')->where(['role'=> 4, 'archive'=>0])->orderBy('id', 'desc')->get();
    }
    public static function deleteParent($id)
    {
        DB::table('users')->where(['role'=>4, 'archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findParent($id)
    {
        return DB::table('users')->where(['role'=>4, 'archive' => 0, 'id' => $id])->first();
    }

    static public function getParentMyStudent($parent_id)
    {
        return DB::table('users')->where(['role'=>3, 'archive' => 0, 'parent_id' => $parent_id])->get();
    }
}
