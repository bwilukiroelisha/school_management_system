<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AssignmentModel extends Model
{
    use HasFactory;

    protected $table = 'assignments';
    static public function getAssignment()
    {
        return DB::table('assignments')->where(['archive'=>0])->get();
    }
    public static function deleteAssignment($id)
    {
        DB::table('assignments')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findAssignment($id)
    {
        return DB::table('assignments')->where(['archive' => 0, 'id' => $id])->first();
    }

}
