<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClassSubjectModel extends Model
{
    use HasFactory;
    protected $table = 'class_subjects';

    static public function getClassSubjectList()
    {
        return DB::table('class_subjects')->where('archive', 0)->orderBy('id', 'desc')->get();
    }
    public static function deleteClassSubject($id)
    {
        DB::table('class_subjects')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findClassSubject($id)
    {
        return DB::table('class_subjects')->where(['archive' => 0, 'id' => $id])->first();
    }
}
