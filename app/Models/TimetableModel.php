<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TimetableModel extends Model
{
    use HasFactory;

    protected $table = 'timetable';

    static public function getTimetable()
    {
        return DB::table('timetable')
            ->join('week_days', 'week_days.id', '=', 'timetable.day_id')
            ->join('subjects', 'subjects.id', '=', 'timetable.subject_id')
            ->join('classes', 'classes.id', '=', 'timetable.class_id')
            ->where('timetable.archive','=','0')
            ->select('timetable.*','week_days.name as day', 'subjects.name as subject','classes.name as class')
            // ->groupBy('timetable.day_id')
            ->orderBy('timetable.day_id','asc')
            ->get();

    }

    public static function deleteItem($id)
    {
        DB::table('timetable')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findItem($id)
    {
        return DB::table('timetable')->where(['archive' => 0, 'id' => $id])->first();
    }

    static function viewTimetable($subjec_id)
    {
        return DB::table('timetable')
            ->join('week_days', 'week_days.id', '=', 'timetable.day_id')
            ->join('subjects', 'subjects.id', '=', 'timetable.subject_id')
            ->join('classes', 'classes.id', '=', 'timetable.class_id')
            ->where('timetable.subject_id', '=', $subjec_id)
            ->where('timetable.archive','=','0')
            ->select('timetable.*','week_days.name as day', 'subjects.name as subject','classes.name as class')
            ->orderBy('timetable.day_id','asc')
            ->get();

    }

    static public function showExam()
    {
        return DB::table('exam_schedules')
            ->join('subjects', 'subjects.id', '=', 'exam_schedules.subject_id')
            ->join('classes', 'classes.id', '=', 'exam_schedules.class_id')
            ->join('grades', 'grades.id', '=', 'exam_schedules.pass_marks')
            ->where('exam_schedules.archive','=','0')
            ->select('exam_schedules.*','subjects.name as subject', 'classes.name as class','grades.name as grade')
            ->orderBy('exam_schedules.id','desc')
            ->get();

    }


    static public function findExam($id)
    {
        return DB::table('exam_schedules')->where(['archive' => 0, 'id' => $id])->first();
    }

    public static function deleteExam($id)
    {
        DB::table('exam_schedules')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }


}
