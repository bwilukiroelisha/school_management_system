<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticeRecipientModel extends Model
{
    use HasFactory;
    protected $table = 'notice_recipients';
    public function noticeBoard()
    {
        return $this->belongsTo(NoticeBoardModel::class);
    }

}
