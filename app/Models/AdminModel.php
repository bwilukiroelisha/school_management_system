<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminModel extends Model
{
    use HasFactory;
    protected $table = 'users';

    static public function getAdminList()
    {
        return DB::table('users')->where(['role'=> 1, 'archive'=>0])->orderBy('id', 'desc')->get();
    }
    public static function deleteAdmin($id)
    {
        DB::table('users')->where(['role'=> 1,'archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findAdmin($id)
    {
        return DB::table('users')->where(['role'=> 1,'archive' => 0, 'id' => $id])->first();
    }

    static public function getUserRole()
    {

        $id = Auth::user()->role;

        return DB::table('roles')->where('id', $id)->value('name');
    }

    static public function checkEmail($email)
    {
        return DB::table('users')->where(['email'=>$email,'archive'=>0,'status'=>0])->first();
    }


    static public function searchUser()
    {
        return DB::table('users')
                ->join('roles', 'users.role', '=', 'roles.id')
                ->where('users.archive','=','0')
                ->select('users.*','roles.name as role_name')
                ->get();

    }

}
