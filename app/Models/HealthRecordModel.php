<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HealthRecordModel extends Model
{
    use HasFactory;


    protected $table = 'health_records';

    static public function getList()
    {
        return DB::table('users')
                    ->join('health_records', 'users.id', '=', 'health_records.student_id')
                    ->join('health_measurements', 'health_records.measurement_id', '=', 'health_measurements.id')
                    ->select('users.id as student_id',
                        'users.fname',
                        'users.mname',
                        'users.lname',
                        'users.gender',
                        'health_measurements.created_by as created_by',
                        'health_measurements.created_at as created_at',
                        'health_measurements.name as measurement_name',
                        'health_measurements.status as status',
                        'health_measurements.id as id',
                        'health_records.value as measurement_value')
                    ->where('health_records.archive', 0)
                    ->orderBy('health_measurements.id', 'desc')
                    ->get()
                    ->groupBy('student_id');
    }
    public static function deleteHRecord($id)
    {
        DB::table('health_records')->where(['archive' => 0, 'student_id' => $id])->update(['archive' => 1]);
    }

    static public function findHRecord($id)
    {
        return DB::table('health_records')->where(['archive' => 0, 'student_id' => $id])->get();
    }

}
