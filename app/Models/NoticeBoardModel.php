<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NoticeBoardModel extends Model
{
    use HasFactory;

    protected $table = 'notice_boards';

    public function recipients()
    {
        return $this->hasMany(NoticeRecipientModel::class);
    }

    static public function getList()
    {
        return DB::table('notice_boards')
            ->join('notice_recipients', 'notice_boards.id', '=', 'notice_recipients.notice_id')
            ->join('roles', 'notice_recipients.recipient_type', '=', 'roles.id')
            ->select(
                'notice_boards.id as notice_id',
                'notice_boards.title',
                'notice_boards.notice_date',
                'notice_boards.published_date',
                'notice_boards.message',
                'notice_boards.attachment',
                'notice_boards.status',
                'roles.name as role_name',
                'notice_recipients.recipient_type',
                'notice_boards.created_by as created_by',
                'notice_boards.created_at as created_at',
                'notice_boards.updated_at as updated_at'
            )
            ->where('notice_boards.archive', 0)
            ->get()
            ->groupBy('notice_id');
    }

    public static function deleteNotice($id)
    {
        DB::table('notice_boards')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findNotice($id)
    {
        return DB::table('notice_boards')->where(['archive' => 0, 'id' => $id])->first();
    }
}
