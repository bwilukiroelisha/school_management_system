<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubjectModel extends Model
{
    use HasFactory;
    protected $table = 'subjects';

    static public function getSubjectList()
    {
        return DB::table('subjects')->where('archive', 0)->orderBy('id', 'desc')->get();
    }
    public static function deleteSubject($id)
    {
        DB::table('subjects')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findSubject($id)
    {
        return DB::table('subjects')->where(['archive' => 0, 'id' => $id])->first();
    }


    static public function getWeekDays()
    {
        return DB::table('week_days')->where(['archive'=>0])->orderBy('id', 'asc')->get();
    }

    static public function findStudentSubject($subject_id)
    {
        return DB::table('subjects')->where(['archive' => 0, 'id' => $subject_id])->get();
    }

    public function students()
    {
        return $this->belongsToMany(StudentModel::class, 'student_subjects');
    }

}
