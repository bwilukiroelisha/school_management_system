<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TeacherModel extends Model
{
    use HasFactory;
    protected $table = 'users';
    static public function getTeacherList()
    {
        return DB::table('users')->where(['role'=> 2, 'archive'=>0])->orderBy('id', 'desc')->get();
    }
    public static function deleteTeacher($id)
    {
        DB::table('users')->where(['role'=>2, 'archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function findTeacher($id)
    {
        return DB::table('users')->where(['role'=>2,'archive' => 0, 'id' => $id])->first();
    }


    static public function getTeacherSubject($teacher_id)
    {
        return DB::table('subjects')
                ->join('teacher_subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
                ->join('users', 'users.id', '=', 'teacher_subjects.teacher_id')
                ->where('users.id', $teacher_id)
                ->where('teacher_subjects.archive','=','0')
                ->where('teacher_subjects.status','=','0')
                ->select('subjects.*')
                ->get();

    }

    static public function myStudent($teacher_id)
    {
        return DB::table('users')
                ->join('teacher_students', 'users.id', '=', 'teacher_students.student_id')
                ->where('teacher_students.teacher_id', $teacher_id)
                ->where('teacher_students.archive','=','0')
                ->where('teacher_students.status','=','0')
                ->select('users.*')
                ->get();

    }


    static public function mySubject($teacher_id)
    {
        return DB::table('subjects')
                ->join('teacher_subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
                ->where('teacher_subjects.teacher_id', $teacher_id)
                ->where('teacher_subjects.archive','=','0')
                ->where('teacher_subjects.status','=','0')
                ->select('subjects.*')
                ->get();

    }


    public static function deleteTeacherSubject($id)
    {
        DB::table('teacher_subjects')->where(['archive' => 0, 'id' => $id])->update(['archive' => 1]);
    }

    static public function updateStudent($id)
    {
        $condtion1 = ['status' => 0 ];
        $condtion2 = ['status' => 1 ];

        if($condtion1)
        {
            DB::table('teacher_students')->where(['archive' => 0, 'student_id' => $id])->update($condtion1);
        }
        else
        {
            DB::table('teacher_students')->where(['archive' => 0, 'student_id' => $id])->update($condtion2);
        }
    }

}
